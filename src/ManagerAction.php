<?php

namespace Commerce\Manager;


use Commerce\Manager\Controller\ManagerController;
use JetBrains\PhpStorm\NoReturn;
use Kubomikita\Flash;
use Nette\Http\FileUpload;
use Nette\Utils\ArrayHash;
use Nette\Utils\FileSystem;
use PageData;
use TemplateData;
use Auth;
use Content;
use Registry;
use StavObjednavky;
use Config;
use ObjednavkaStav;
use Objednavka;
use Timestamp;
use LogAction;

class ManagerAction extends ManagerController {

	#[NoReturn]
	public function actionPagedatasave(ArrayHash $data){
		$p = new PageData((int)$data["id"]);
		$p->params = (array) $data["data"];
		$p->save();
		$this->redirect("mod.rs.pages.php?action=edit&id=".$p->page);
	}

	public function actionPagedatadelete(int $id, string $uri, string $uri_action, int $uri_id){
		$p = new PageData($id);
		$p->delete();
		$this->redirect($uri."?action=".$uri_action."&id=".$uri_id);
	}
	public function actionPagedatashow(int $id, string $uri, string $uri_action, int $uri_id){
		$p = new PageData($id);
		$p->show = "all";
		$p->save();
		$this->redirect($uri."?action=".$uri_action."&id=".$uri_id);
	}
	public function actionPagedatahide(int $id, string $uri, string $uri_action, int $uri_id){
		$p = new PageData($id);
		$p->show = "hide";
		$p->save();
		$this->redirect($uri."?action=".$uri_action."&id=".$uri_id);
	}
	public function actionPagedatamoveup(int $id, string $uri, string $uri_action, int $uri_id){
		$p = new PageData($id);
		$p->moveUp();
		$this->redirect($uri."?action=".$uri_action."&id=".$uri_id);
	}
	public function actionPagedatamovedown(int $id, string $uri, string $uri_action, int $uri_id){
		$p = new PageData($id);
		$p->moveDown();
		$this->redirect($uri."?action=".$uri_action."&id=".$uri_id);
	}
	public function actionPagedatalang(int $id, string $lang, string $uri, string $uri_action, int $uri_id){

		if($lang == "all"){
			$lang = null;
		}
		$p = new PageData($id);
		$p->lang = $lang;
		$p->save();
		$this->redirect($uri."?action=".$uri_action."&id=".$uri_id);

	}

	#[NoReturn]
	public function actionTemplatedatasave(ArrayHash $X){
		$p = new TemplateData((int)$X["id"]);
		$p->params = (array) $X["data"];
		$p->save();
		$this->redirect("mod.rs.template.php#box_".$p->box);
		//$this->redirect("mod.rs.template.php?action=edit&id=".$p->template);
	}


	public function actionTemplatedatadelete(int $id, string $uri){
		$p = new TemplateData($id);
		$p->delete();
		$this->redirect("mod.rs.template.php#box_".$p->box);
		///$this->redirect($X["uri"]."?action=".$X["uri_action"]."&id=".$X["uri_id"]);
	}

	public function actionTemplatedatashow(int $id, string $uri){
		$p = new TemplateData($id);
		$p->show = "all";
		$p->save();
		$this->redirect("mod.rs.template.php#box_".$p->box);
	}

	public function actionTemplatedatahide(int $id, string $uri){
		$p = new TemplateData($id);
		$p->show = "hide";
		$p->save();
		$this->redirect("mod.rs.template.php#box_".$p->box);
	}

	public function actionTemplatedatamoveup(int $id, string $uri/*, string $uri_action, int $uri_id*/){
		$p = new TemplateData($id);
		$p->moveUp();
		$this->redirect("mod.rs.template.php#box_".$p->box);
		//$this->redirect($uri."?action=".$uri_action."&id=".$uri_id);
	}
	public function actionTemplatedatamovedown(int $id, string $uri/*, string $uri_action, int $uri_id*/){
		$p = new TemplateData($id);
		$p->moveDown();
		$this->redirect("mod.rs.template.php#box_".$p->box);
		//$this->redirect($uri."?action=".$uri_action."&id=".$uri_id);
	}

	public function actionTemplatedatalang(int $id, string $lang, string $uri){

		if($lang == "all"){
			$lang = null;
		}
		$p = new TemplateData($id);
		$p->lang = $lang;
		$p->save();
		$this->redirect("mod.rs.template.php#box_".$p->box);
		//$this->redirect($X["uri"]."?action=".$X["uri_action"]."&id=".$X["uri_id"]);

	}

	function actionAjaxLoadTovar(string $search){
		$s = new \TovarSearchResult();
		$s->fulltext = $search;
		$s->aktivny = 0;
		$s->indexable = 0;

		$json = [];
		if(mb_strlen($search) > 2) {
			foreach ( $s->netteQuery() as $q ) {
				$T      = new \Tovar( $q->tovar_id );
				$json[] = [ "id" => $T->id, "text" => $T->kod_systemu." | ". (string) $T->nazov ];
			}
		}
		
		header('Content-Type: application/json');
		echo \Nette\Utils\Json::encode(["items" => $json]);
		$this->terminate();
	}

	function actionStavyView(int $o_id){
		if(!Auth::ACL('orders_view')){ return false; };

		Content::$Mainbox.='<div class="in"><table class="list">';
		Content::$Mainbox.='<tr><th>dátum</th><th>stav</th><th>email</th><th>zobraz.</th><th colspan="2">komentár</th><th>&nbsp;</th></tr>';
		$O=new \Objednavka($o_id);

		foreach($O->stav_list(1) as $Stav){
			/* @var $Dph \Dph */
			$rowClass=array('listrow');
			//if($Stav->hidden){ $rowClass[]='disabledrow'; };
			$out='';
			$out.='<tr class="'.join(' ',$rowClass).'">';
			$out.='<td class="">'.$Stav->datum->show('d.m.Y H:i').'</td>';
			$out.='<td class="b">'.$Stav->stav()->nazov.'</td>';
			$out.='<td><img src="'.(isset($Stav->metadata["message_sended"]) && $Stav->metadata["message_sended"] ?'img/16/bool_1.png':'img/16/bool_0.png').'"></td>';
			$out.='<td><img src="'.($Stav->hidden?'img/16/hidden.png':'img/16/visible.png').'"></td>';
			//$out.='<td><img src="'.($Stav->hidden_comment?'img/16/hidden.png':'img/16/visible.png').'" '.($Stav->hidden?'style="display: none;"':'').'></td>';
			$out.='<td class="small">'.nl2br($Stav->comment);
			bdump($Stav);
			if(!empty(($prilohy = $Stav->getAttachments()))) {
				$out .= "<div style='margin-top: 15px;margin-bottom: 10px'>Prílohy:</div>";
				foreach ($prilohy as $attachment) {
					$out .= '<div style="font-weight: bold"><img src="img/rse/img_file.gif" width="14"> ' . $attachment[1] . '</div>';
				}
			}
			$out.='</td>';
			$out.='<td class="btn">';
			if(\Auth::ACL('orders_process')){ $out.='<img src="img/16/delete.png" style="cursor:pointer;" onclick="location.href=\'mod.objednavky.php?action=stavy_del&o_id='.$O->id.'&id='.$Stav->id.'\';">'; };
			$out.='</td>';
			$out.='</tr>';
			\Content::$Mainbox.=$out;
		};
		\Content::$Mainbox.='</table></div>';


		if(Auth::ACL('orders_process')){
			Content::$Mainbox.='<div class="formbox" style="margin: 10px;"><form method="POST" action="mod.objednavky.php" enctype="multipart/form-data">';
			Content::$Mainbox.='<input type="hidden" name="o_id" value="'.$O->id.'">';
			Content::$Mainbox.='<input type="hidden" name="action" value="stavy_add">';
			Content::$Mainbox.='<div class="title">'.LangStr('PRIDAŤ').'</div>';
			//Content::$Mainbox.=LangStr('dátum:').' '.Input('text','datum',Timestamp()->show('d.m.Y'))->cssClass('textinput dateinput')->width(100);

			Content::$Mainbox.='<div style="margin-top:.5rem;margin-bottom: .5rem;"><div class="small">'.LangStr('stav:').'</div> ';
			Content::$Mainbox.=Input('select','stav','',  StavObjednavky::fetchSelect('...'))->cssClass('textinput')->width(150)->domId('stav_select')."</div>";

			Content::$Mainbox.='<div  style="margin-bottom: .5rem;"><div class="small">komentár:</div><textarea class="textinput" name="koment" rows=6 cols=120  id="stav_koment"></textarea></div>';
			if(Config::sendSms){ Content::$Mainbox.='<div ><div class="small">SMS:</div><textarea class="textinput" name="sms" rows=6 cols=120  id="stav_sms"></textarea></div>'; };
			Content::$Mainbox.="<div style='margin-bottom: .5rem;'><div class='small'>prílohy:</div><input type='file' name='prilohy[]' multiple></div>";
			Content::$Mainbox.='<div style="margin-bottom: .5rem;"><input type="checkbox" name="stav_silent_no" value="1" checked> upozorniť zákazníka mailom príp. SMS &nbsp;&nbsp; <input type="checkbox" name="stav_hidden_no" value="1" checked> zobraziť zákazníkovi v profile  &nbsp;&nbsp; <input type="checkbox" name="stav_comment_hidden_no" value="1" checked> zobraziť zákazníkovi v profile aj komentár</div>';
			Content::$Mainbox.='<input type="submit" value="'.LangStr('Pridať').'" >';
			Content::$Mainbox.='</form>';
			Content::$Mainbox.='<script type="text/javascript">$("#stav_select").change(function(){ $("#stav_koment").load("mod.objednavky.php?action=stavy_text&s="+$(this).val()); $("#stav_sms").load("mod.objednavky.php?action=stavy_sms&s="+$(this).val()); });</script>';
			Content::$Mainbox.='</div>';
		};


		Content::DisplayInline();
		$this->terminate();
	}

	function actionStavyAdd($X){
		if(!Auth::ACL('orders_process')){ return false; };
		if($X["stav"] == ""){
			//Flash::danger("Vyberte stav prosím.");
			header("Location: mod.objednavky.php?action=stavy_view&o_id=".$X['o_id']);
			exit;
		}
		$O=new \Objednavka($X['o_id']);
		if($O->id){
			$OS=new \ObjednavkaStav();
			$OS->stavId=$X['stav'];
			$OS->datum=new Timestamp();
			$OS->comment=$X['koment'];
			$OS->metadata['sms']=$X['sms'];
			$OS->silent=!$X['stav_silent_no'];
			$OS->hidden=!$X['stav_hidden_no'];
			$OS->hidden_comment=!$X['stav_comment_hidden_no'];
			//dumpe($X);
			if(!empty($X->prilohy)){
				$dir = TEMP_DIR."attachments/";
				FileSystem::createDir($dir);
				/** @var FileUpload $priloha */
				foreach ($X->prilohy as $priloha )
				{
					if($priloha?->isOk()){
						$priloha->move($dir.$priloha->getSanitizedName());
						$OS->addAttachment($dir.$priloha->getSanitizedName());
					}
				}
			}
			//dumpe($OS, $OS->getAttachments());
			if(trim($X['sms'])!=''){ $OS->forcesms=1; };
			if($OS->stav()->id){ $rx=$O->stav_add_oo($OS); };
			if($X["stav_silent_no"] and $OS->stav()->listen("ready")){
				if(strlen(trim($OS->comment)) > 0) {
					$mail = $OS->sendMessage(
						(strlen(trim($O->klient->kontakt["email"])) > 0 ? $O->klient->kontakt["email"] : $O->klient->username),
						LangStr( 'objednávka č. ' ) . $O->cislo . ' - ' . $OS->stav()->nazov,
						$OS->comment
					);
				}
			}

			/*if($X["stav_silent_no"] and $OS->stav()->listen("ready")){
				$M=new Mail();
				$M->from=Params::val('e_name_from').' <'.Params::val('e_mail_from').'>';
				$M->to=$O->klient->kontakt['email'];
				$M->subject=LangStr('objednávka č. ').$O->cislo.' - '.$OS->stav()->nazov;
				$M->content_type='text/html';
				ob_start();
				$CONTENT='<h3>Objednávka č. '.$O->cislo.'</h3><pre>'.$OS->comment.'</pre>';
				include(__DIR__.'/../mail/template.php');
				$M->text=ob_get_contents();
				ob_end_clean();
				if(trim($OS->comment)){ $M->send(); };
			}*/
			//$rx=$O->stav_add(new Timestamp(), $X['stav'], $X['koment'],$X['sms']);
		};
		LogAction::add("[user] pridal stav - ".$OS->stav()->nazov." - objednávke číslo [objectId]",null,null,$O->cislo);
		if($rx['vybavene']){
			echo(Content::htmlPlainHeader());
			echo('<script type="text/javascript">window.parent.$("#vybavene_src").attr("checked","checked").trigger("change"); location.href="mod.objednavky.php?action=stavy_view&o_id='.$X['o_id'].'";</script>');
			echo(Content::htmlPlainFooter());
		} else {
			header("Location: mod.objednavky.php?action=stavy_view&o_id=".$X['o_id']);
		};
		exit;
	}
}