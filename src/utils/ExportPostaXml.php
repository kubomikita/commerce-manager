<?php
namespace Commerce\Manager\Utils;
class ExportPostaXml{
	public $obj_id,$info,$xmlVal;
	private $db;

	public function __construct($obj_id) {
		$this->obj_id=$obj_id;
		//$this->connect();
		$this->db = \CommerceDB::$DB;
		$this->info=$this->getObj();

	}
	public function connect(){
		$conn=new mysqli(DBSERVER,DBUSER,DBPASS,DBNAME);

		if ($conn->connect_error){
			throw new Exception($conn->connect_errno." ".$conn->connect_error,0);
		}
		$conn->query("SET NAMES UTF8");
		$this->db=$conn;
	}
	public function getObj(){
		$q=$this->db->query("SELECT platba,klient,klient_prevadzka,_cache_suma,cislo,metadata FROM ec_objednavky WHERE id='$this->obj_id' and _cache_logistika_code='posta' and xml='0'");
		$r=$q->fetch_Assoc();
		return $r;
	}
	public function getHmotnost(){
		$q= $this->db->query("SELECT tovar,variant,mnozstvo_objednane FROM ec_objednavky_data WHERE objednavka = '$this->obj_id'");
		$posible = array(10,9,6,8);
		$hmotnost=0;
		while($r=$q->fetch_assoc()){
			$var = unserialize($r["variant"]);
			/*foreach($var as $k=>$v){

			}*/
			//var_dump($var,$r);
			foreach($posible as $k){
				if(!isset($var[$k])){
					$var[$k] = null;
				}
			}
			//var_dump($var);
			//echo "SELECT metadata FROM ec_tovar_varianty_data where cls='sklad' and tovar='$r[tovar]' and variant='".serialize($var)."' ";
			$qq=$this->db->query("SELECT variant,metadata FROM ec_tovar_varianty_data where cls='sklad' and tovar='$r[tovar]'");
			while($rr=$qq->fetch_assoc()){

				$cond = array_diff($var,unserialize($rr["variant"]));
				//var_dump($cond,empty($cond),count($cond));
				//echo '<hr>';
				if(empty($cond)){
					//echo "sedi ";
					$meta = unserialize($rr["metadata"]);
					$hmotnost+=((int) $r["mnozstvo_objednane"]* (float) $meta["hmotnost"]);
					break;
				}

				/*if(empty(array_diff($var,unserialize($rr["variant"])))){
					echo "sedi ".$rr["variant"]."<br>";
				}*/
				/*

				if(empty(array_diff($var,unserialize($rr["variant"])))){
					$hmotnost+=$meta["hmotnost"];
				}*/

			}
		}
		//var_dump(($hmotnost/1000));
		return round(($hmotnost/1000),2);
	}
	/*public function getMeta(){
		$q=$this->db->query("SELECT metadata FROM ec_objednavky WHERE id='".$this->info["platba"]."'");
		$r=$q->fetch_Assoc();
		$platba=unserialize($r["metadata"]);
		return $platba["skratka"];
	}*/
	public function getPlatba(){
		$q=$this->db->query("SELECT metadata FROM ec_sposoby_platby WHERE id='".$this->info["platba"]."'");
		$r=$q->fetch_Assoc();
		$platba=unserialize($r["metadata"]);
		return $platba["skratka"];
	}
	public function getAdresa(){
		$q=$this->db->query("SELECT kontakt,nazov,fa_adresa FROM ec_partneri WHERE id='".$this->info["klient"]."'");
		$r=$q->fetch_assoc();
		//var_dump($r);
		$result= array_merge(array("nazov"=>$r["nazov"]),unserialize($r["kontakt"]));
		if(!array_key_exists("mesto",unserialize($r["kontakt"])) and !array_key_exists("psc",unserialize($r["kontakt"]))){
			$result= array_merge($result,unserialize($r["fa_adresa"]));
		}
		if($this->info["klient_prevadzka"]!=0){
			$q=$this->db->query("SELECT nazov,adresa,metadata,kontakt FROM ec_partneri_prevadzky WHERE partner_id='".$this->info["klient"]."' and id='".$this->info["klient_prevadzka"]."'");
			$r=$q->fetch_assoc();
			$adresa=unserialize($r["adresa"]);
			$meta=unserialize($r["metadata"]);
			$kontakt=  unserialize($r["kontakt"]);
			$result = array_merge($result,$adresa);
			//$result = array_merge($result,$meta);

			$result = array_merge($result,$kontakt);
			$result["meno"]=$r["nazov"];$result["priezvisko"]="";
		}
		return $result;
	}
	public function updateXML($id,$kod=array()){
		if(!empty($kod)) $add=",podcislo='$kod[orig]',podcislo_full='$kod[full]'";
		$this->db->query("UPDATE ec_objednavky SET xml='$id'$add WHERE id='$this->obj_id'");
		echo $this->db->error;
	}
	public function update($query){
		$this->db->query($query);
	}
	public function selectSameXml(){
		$q=$this->db->query("SELECT xml,xml_file FROM ec_objednavky WHERE id='$this->obj_id'");
		$r=$q->fetch_Assoc();
		$ids=array();
		$ids["xml"] = $r["xml"];
		$ids["xml_file"] = $r["xml_file"];
		$q1=$this->db->query("SELECT id FROM ec_objednavky WHERE id!='$this->obj_id' and xml_file='$r[xml_file]'");
		while($r1=$q1->fetch_Assoc()){
			//array_push($ids,$r1[id]);
			$ids[]=$r1["id"];
		}
		return array_merge($r,$ids);
	}
	public function deleteFromDB($xml,$xml_file){
		$this->db->query("UPDATE ec_objednavky SET xml='0', xml_file='',podcislo='',podcislo_full='' WHERE xml='$xml' and xml_file='$xml_file'");
	}
	public function getPodacieCislo(){
		$q=$this->db->query("SELECT * FROM ec_objednavky WHERE xml='1' and podcislo!='' and (podcislo > 09444833  and podcislo < 09449833) ORDER BY podcislo DESC LIMIT 10");
		echo $this->db->error;
		$r=$q->fetch_assoc();
		bdump($r);
		if($r == null || $r["podcislo"]==""){
			//$start="04362451";
			$start="05996301";
		} else {
			$start="0".($r["podcislo"]+1)."";
		}
		//var_dump($r[podcislo]);
		//var_dump($start);
		$vaha="86423597";
		//echo $start;
		$sucet=0;
		for($i=0;$i<8;$i++){
			$q=$start[$i]*$vaha[$i];
			//echo $kod[$i]." * ".$vaha[$i]." = $q<br>";
			$sucet+=$q;
		}
		$zvysok=$sucet%11;
		$kc=(11-$zvysok);
		if($kc==10) $kc=0;
		if($kc==11) $kc=5;
		return array("full"=>"ZB".$start.$kc."SK","orig"=>$start);

	}

}