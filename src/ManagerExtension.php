<?php
declare(strict_types=1);

namespace Commerce\Bridges\ManagerDI;

use Commerce\ControllerFactory;
use Commerce\Manager\Controller\ManagerController;
use Commerce\Manager\Manager;
use Commerce\Manager\ManagerAction;
use Horti\Manager\Controller\DefaultController;
use Kubomikita\Commerce\Application;
use Kubomikita\Commerce\Configurator;
use Kubomikita\Commerce\ConfiguratorInterface;
use Kubomikita\Commerce\Model\Loader;
use Kubomikita\Commerce\RegistryProvider;
use Nette;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use Nette\Utils\Finder;

class ManagerExtension extends Nette\DI\CompilerExtension {

	protected string $dir = APP_DIR.'core/manager/controller';
	#protected string $defaultDir = __DIR__."/Manager/controller";

	public function getConfigSchema(): Schema
	{
		return Expect::structure([
			'redirectWww' => Expect::bool(true),
			'controllers' => Expect::listOf('string'),
			'dir' => Expect::string(__DIR__."/controller")
		]);
	}


	public function loadConfiguration()
	{
		$this->dir = $this->config->dir;
		
		$builder = $this->getContainerBuilder();
		
		if(class_exists(Manager::class)) {
			$builder->addDefinition($this->prefix('manager'))->setFactory(Manager::class)->addSetup("setRedirectWww", [$this->config->redirectWww]);
			$builder->addDefinition($this->prefix('managerAction'))->setFactory(ManagerAction::class);
		}
	}

	public function beforeCompile()
	{
		$builder = $this->getContainerBuilder();

		$this->name = 'manager.controller';

		// Check first config added controllers
		foreach($this->config->controllers as $controller){
			$reflection = new \ReflectionClass($controller);

			if(!$reflection->isSubclassOf(ManagerController::class)){
				throw new Nette\DI\InvalidConfigurationException('Class \''.$controller.'\' must be instance of \''.ManagerController::class.'\'');
			}
			$classShortName = $reflection->getShortName();
			$className = $reflection->getName();
			$serviceName = $this->formatServiceName($classShortName);

			$builder->addDefinition($serviceName)->setFactory($className)->addTag($classShortName);
		}

		// Check library controllers
		if(file_exists($this->dir)) {
			foreach (Finder::find("*Controller.php")->in($this->dir) as $Controller) {

				$classShortName = str_replace(".php", "", $Controller->getFilename());
				$className      = Manager::CONTROLLER_NAMESPACE . $classShortName;
				$serviceName    = $this->formatServiceName($classShortName);

				if (class_exists($className) && ! $builder->hasDefinition($serviceName)) {
					$reflection = new \ReflectionClass($className);
					if ($reflection->isSubclassOf(ManagerController::class)) {


						$loaded[$classShortName] = $serviceName;
						$builder->addDefinition($serviceName)
						        ->setFactory($className)
						        ->addTag($classShortName);
					}
				}
			}
		}
	}
	private function formatServiceName($classShortName) : string
	{
		return $this->prefix(Nette\Utils\Strings::lower(str_replace("Controller","", $classShortName)));
	}
}