<?php
if(!function_exists("init_auth")) {
	/** @deprecated */
	function init_auth(): void
	{

	}
}
if(!function_exists("commit_action")) {
	/** @deprecated */
	function commit_action()
	{

	}
}
if(!function_exists("init_auth_ajax") && false) {
	/** @deprecated */
	function init_auth_ajax(\Nette\Security\User $User)
	{
		if ($User->isLoggedIn()) {
			$Identity       = $User->getIdentity();
			Auth::$User     = new User($User->getId());
			Auth::$auth     = 1;
			Auth::$userId   = Auth::$User->id;
			Auth::$userName = Auth::$User->username;
			Auth::$mainPage = Auth::$User->mainpage;
			Auth::$identity = $Identity;
			bdump($User->getStorage(), "User STORAGE");
		}
	}
}
if( !class_exists("AJAX")) {
	class AJAX
	{
		private static $operator = array();

		public static function register($command)
		{
			if (func_num_args() == 2) {
				AJAX::$operator[$command] = func_get_arg(1);
			};
			if (func_num_args() == 3) {
				AJAX::$operator[$command] = array(func_get_arg(1), func_get_arg(2));
			};
		}

		public static function call($command)
		{
			if (isset(AJAX::$operator[$command])) {
				call_user_func(AJAX::$operator[$command]);
			};
		}

	}
}