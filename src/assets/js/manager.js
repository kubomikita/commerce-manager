//console.log("manager.js");

var working=0;
function work_start(){
	working++;
	if($(document).find("#ajax-spinner").length === 0) {
		spinner = $('<div></div>', {id: "ajax-spinner", style: "visibility:hidden;display:none;"});
		spinner.append('<div id="spinner" class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>');
		spinner.append('<div>Prosím, počkajte chvíľu...</div>');
		spinner.appendTo("body");
		console.log(spinner);
	}

	let el = $(window.document).find("#ajax-spinner");
	console.log(el);
	el.css({
		visibility: "visible",
		display: "block",
	});
	//el.attr("data-auto-off", autoTurnOff);
};
function work_done(){
	working--;
	if(working<0){ working=0; };
	if(working==0){
		$("#ajax-spinner").css({
			visibility: "hidden",
			display: "none",
		});
	};
};

function urlencode(str) {
	return escape(str).replace(/\+/g,'%2B').replace(/%20/g, '+').replace(/\*/g, '%2A').replace(/\//g, '%2F').replace(/@/g, '%40');
}

function psc_lookup_protein(psc){
	var url="mod.psc.php?action=show_psc_ulica&psc="+psc+"&iframe=1";
	$("#iframe_psc").attr("src",url);
}

function psc_lookup(psc){
	var url="mod.psc.php?action=show_psc_ulica&psc="+psc;
	/*
	var fopts=$.extend({},fancyboxDefaults,{'type' : 'iframe' , 'hideOnOverlayClick' : false, 'hideOnContentClick' : false, 'enableEscapeButton': false });
	$.extend(fopts,{'width':parseInt(600)});
	$.extend(fopts,{'height':parseInt(400)});
	$.fancybox(url,fopts);
	*/
	var nw = window.open(url,"","status=0,toolbar=0,menubar=0,location=0,height=400,width=600");
}

function sms_lookup(tel){
	var url="mod.sms.php?action=new_sms&telefon="+tel;
	var fopts=$.extend({},fancyboxDefaults,{'type' : 'iframe' , 'hideOnOverlayClick' : false, 'hideOnContentClick' : false, 'enableEscapeButton': false });
	$.extend(fopts,{'width':parseInt(400)});
	$.extend(fopts,{'height':parseInt(250)});
	$.fancybox(url,fopts);
	//var nw = window.open(url,"","status=0,toolbar=0,menubar=0,location=0,height=400,width=600");
}

var cache=Math.random(1000000);

var fancyboxDefaults = {
	'transitionIn'	:	'elastic',
	'transitionOut'	:	'elastic',
	'speedIn'		:	600,
	'speedOut'		:	200,
	'overlayOpacity' : 0.8,
	'overlayColor' : '#333333',
	'titlePosition' : 'over'
};

$(document).ready(function (){
	$(".hoverable").mouseover(function(){ $(this).addClass("hover"); }).mouseout(function(){ $(this).removeClass("hover"); });

	var $ribbonTop=$(".r_top")
	$ribbonTop.mouseover(function(){ $(this).addClass("rh_top"); }).mouseout(function(){ $(this).removeClass("rh_top"); });
	$ribbonTop.click(function(){
		$ribbonTop.removeClass("ra_top");
		var $this=$(this);
		$this.addClass("ra_top");
		$(".r_bar").removeClass("r_bar_active");
		$("#ribbon_"+$this.attr("rel")).addClass("r_bar_active");
		$.ajax({ url:"rpc.php?m=menu_tab&t="+$this.attr("rel") , complete: function(){
				if($this.attr("link")!=""){
					work_start(); location.href=$this.attr("link");
				};
			} });

	});

	var $ribbonButton=$(".ribbonbutton");
	$ribbonButton.mouseover(function(){ $(this).addClass("ribbonbutton_hover"); }).mouseout(function(){ $(this).removeClass("ribbonbutton_hover"); });
	$ribbonButton.click(function(){ work_start(); location.href=$(this).attr("href"); });

	$("#logobutton, .commerce-tab").click(function(){ $("#startmenu").toggle(200); });

	var $leftTabs=$(".tabcontrol");
	$leftTabs.mouseover(function(){ $(this).addClass("tabcontrol_hover"); }).mouseout(function(){ $(this).removeClass("tabcontrol_hover"); });
	$leftTabs.click(function(){
		$leftTabs.removeClass("tabcontrol_active");
		$(this).addClass("tabcontrol_active");
		$(".tabcontent").removeClass("tabcontent_active");
		$("#maintab_"+$(this).attr("tab")).addClass("tabcontent_active");
	});

	$("table.list .listrow").mouseover(function(){
		$(this).addClass("listrow_hov").removeClass("listrow_std");
	}).mouseout(function(){
		$(this).addClass("listrow_std").removeClass("listrow_hov");
	});
	$("table.list .activerow").click(function(){
		work_start();
		location.href=$(this).attr("ref");
	});
	$(".noclick").click(function(event){ event.stopPropagation(); });

	$("input.colorpick").ColorPicker({
		onSubmit: function(hsb, hex, rgb, el){ $(el).val(hex).ColorPickerHide(); },
		onBeforeShow: function(){ $(this).ColorPickerSetColor(this.value); }
	}).bind('keyup', function(){ $(this).ColorPickerSetColor(this.value); });;

	$(".langstr_input .languages .ilng_btn").click(function(){
		var $parent=$(this).parents(".langstr_input").first();
		//alert($parent.attr("id"));
		$parent.find(".inputs input").hide();
		$parent.find(".languages .ilng_btn").removeClass("ilng_btn_active");
		$parent.find(".inputs input[ref="+$(this).attr("val")+"]").show();
		$(this).addClass("ilng_btn_active");
	});



	$(".langtxt_input .languages .ilng_btn").click(function(){
		var $parent=$(this).parents(".langtxt_input").first();
		//alert($parent.attr("id"));
		$parent.find(".inputs .textarea").hide();
		$parent.find(".languages .ilng_btn").removeClass("ilng_btn_active");
		$parent.find(".inputs .textarea[ref="+$(this).attr("val")+"]").show();
		$(this).addClass("ilng_btn_active");
	});

	$(".cke").each(function(){
		if($(this).val()==''){ $(this).val('&nbsp;'); };
		$(this).ckeditor( function(){} , { customConfig: "config-inline.js" } );
	});
	$(".cke1").each(function(){
		if($(this).val()==''){ $(this).val('&nbsp;'); };
		$(this).ckeditor( function(){} , { customConfig: "config-simple.js" } );
	});

	$(".buttonbar .button").mouseover(function(){
		$(this).addClass("button_hov");
	}).mouseout(function(){
		$(this).removeClass("button_hov");
	});

	$(".itab").not(".active").each(function (){
		$(this).hide();
	});//.hide(); //$(".itab")..show();
	$(".itabs .itabcontrol a").click(function(){
		$(".itabcontrol a").removeClass('active'); $(this).addClass('active');
		$(".itab").hide(); $($(this).attr('tab')).show();
	});

	$("form.enter").keyup(function(e){
		if(e.keyCode==13){ $(this).submit(); };
	});

	$("a.modal").each(function(){
		var $t=$(this);
		var clickclose = false;
		if($t.hasClass("clickclose")){ clickclose=true; };
		var fopts=$.extend({},fancyboxDefaults,{'type' : 'iframe' , 'hideOnOverlayClick' : clickclose, 'hideOnContentClick' : false, 'enableEscapeButton': clickclose });
		if($t.attr('fw')>0){ $.extend(fopts,{'width':parseInt($t.attr('fw'))});  };
		if($t.attr('fh')>0){ $.extend(fopts,{'height':parseInt($t.attr('fh'))});  };
		$t.fancybox(fopts);
	});

	$("input.autoselect").focus(function(){ this.select(); });

	$("td.keymove input").keyup(function(event){
		if(event.keyCode==40){ $("td.keymove[row="+$(this).parent("td.keymove").attr("next")+"] input").focus(); };
		if(event.keyCode==38){ $("td.keymove[row="+$(this).parent("td.keymove").attr("prev")+"] input").focus(); };
	});

	$(".helpbutton").mouseover(function(){ $(this).find(".helpbox").show(100); }).mouseout(function(){ $(this).find(".helpbox").hide(100); });

	$(".psc").click(function(){
		if($("#pscmode").val()=="protein"){
			psc_lookup_protein($(this).html());
		} else {
			psc_lookup($(this).html());
		};
	});

	$(".sms").click(function(){
		sms_lookup($(this).html());
	});
	$("#exportBalik").click(function(){
		$(".balik").each(function(){
			var id=$(this).val();
			if($(this).attr("disabled") == false){
				if($(this).attr("checked")==false){
					if($("#list_"+id).attr("checked") == false){
						$(this).attr("checked",true);
						$("#list_"+id).attr("disabled",true);
					}
				} else {
					$(this).removeAttr("checked");
					$("#list_"+id).removeAttr("disabled");
				}
			}
		});
	});
	$("#exportList").click(function(){
		$(".list").each(function(){
			var id=$(this).val();
			if($(this).attr("disabled") == false){
				if($(this).attr("checked")==false){
					if($("#balik_"+id).attr("checked") == false){
						$(this).attr("checked",true);
						$("#balik_"+id).attr("disabled",true);
					}
				} else {
					$(this).removeAttr("checked");
					$("#balik_"+id).removeAttr("disabled");
				}
			}
		});
	});
	$(".balik").click(function(){
		var id=$(this).val();
		if($(this).attr("checked")==true){
			$("#list_"+id).attr("disabled",true);
		} else {
			$("#list_"+id).attr("disabled",false);
		}
	});
	$(".list").click(function(){
		var id=$(this).val();
		if($(this).attr("checked")==true){
			$("#balik_"+id).attr("disabled",true);
		} else {
			$("#balik_"+id).attr("disabled",false);
		}
	})
});