<?php

namespace Commerce\Manager;


use Auth;

use Commerce\Manager\Controller\DefaultController;
use Commerce\Manager\Controller\ErrorController;
use Commerce\Manager\Controller\ManagerController;
use Commerce\Manager\Controller\RedirectsController;
use Commerce\Manager\Controller\SignController;
use JetBrains\PhpStorm\NoReturn;
use Kubomikita\Commerce\ConfiguratorInterface;
use Kubomikita\Commerce\UserService;
use MatthiasMullie\Minify\CSS;
use MatthiasMullie\Minify\JS;
use Nette\Application\BadRequestException;
use Nette\Application\Routers\Route;
use Nette\DI\Container;
use Nette\Http\IRequest;
use Nette\Http\IResponse;
use Nette\Security\Authenticator;
use Nette\Security\User;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;
use Nette\Utils\FileSystem;
use Nette\Utils\Strings;

class Manager {

	const CONTROLLER_NAMESPACE = 'Commerce\\Manager\\Controller\\';
	const NAME = 'CommerceManager';
	const VERSION = 2.100;


	protected int $errorReporting = E_ALL;
	protected string $scriptFilename;
	protected ?string $controllerName;
	protected ?string $controllerShortName;
	protected ?ManagerController $controller = null;

	protected ?string $action = null;
	protected array $requestArguments = [];
	protected array $requestPost = [];
	protected array $requestFiles = [];

	protected array $managerActionCheck = [];


	protected bool $redirectWww = true;

	public function __construct(
		protected Container $container,
		protected Authenticator $authenticator,
		protected User $user,
		protected UserService $userService,
		protected IRequest $httpRequest,
		protected IResponse $httpResponse,
		protected ManagerAction $managerAction,
	)
	{
		$this->action = str_replace(["_"],["-"],$this->httpRequest->getQuery('action') ?? $this->httpRequest->getPost('action') ?? 'default');

		$this->scriptFilename = basename($this->httpRequest->getUrl()->getScriptPath());
		if(($requestUrl = $this->httpRequest->getQuery('requestUrl')) !== null){
			//dumpe($this, $requestUrl, basename($requestUrl));
			$this->scriptFilename = basename($requestUrl);
		}
		$this->controllerName = $this->getControllerName();
		$this->controller = $this->getController();

		$this->requestPost = $this->httpRequest->getPost();
		$this->requestFiles = $this->httpRequest->getFiles();
		$this->requestArguments = $this->httpRequest->getQuery() + $this->requestPost + $this->requestFiles;



		$this->authenticator->setNamespace($this->authenticator::NAMESPACE_ADMIN);
		$this->user->setAuthenticator($this->authenticator);
		$this->user->getStorage()->setNamespace($this->userService::NAMESPACE_ADMIN);

		$this->userService->checkCookie($this->userService::NAMESPACE_ADMIN);

	}


	#[NoReturn]
	public function run():void
	{
		error_reporting($this->errorReporting);
		$this->exposeBigFiles();
		$langStr = new \LangStr();

		$this->checkCssFiles();
		$this->checkJsFiles();

		\Flash::options([
			"types" => [
				"success" => ["icon" => "fa-solid fa-circle-check",],
				"danger" => ["icon" => "fa-solid fa-circle-xmark"],
				"info" => ["icon" => "fa-solid fa-circle-info"],
				"warning" => ["icon" => "fa-solid fa-circle-exclamation"],
			]
		]);


		$this->initAuth();

		try {
			$this->processRequest();
		} catch (BadRequestException $e){
			if($this->container->getByType(ConfiguratorInterface::class)->isDebugMode()){
				throw $e;
			}

			$this->action = "default";
			$this->controllerName = ErrorController::class;
			$this->controller = $this->getController();

			$this->httpResponse->setCode($e->getCode());
			$this->processRequest();
		}
		exit;
	}

	private function processRequest() : void
	{


		$method = 'action'.Route::path2presenter($this->action);

		$alreadyServed = false;
		//dump($this->controller,$method, method_exists($this->controller, $method), file_exists($this->controller->formatTemplateFile()),$this->controller->formatTemplateFile());
		if($this->controller !== null && (($actionExists = method_exists($this->controller, $method)) || file_exists($this->controller->formatTemplateFile())) ){
			$this->controller->startup();
			if($this->httpRequest->getMethod() === 'GET' && $actionExists) {
				$reflectionMethod = new \ReflectionMethod($this->controller, $method);

				$routerArgs       = $this->requestArguments;
				$actionArgs       = [];
				$actionParameters = $reflectionMethod->getParameters();

				foreach ($actionParameters as $param) {
					if (isset($routerArgs[$param->name])) {
						if (empty($routerArgs[$param->name]) && $routerArgs[$param->name] !== "0" && ! $param->isOptional()) {
							throw new BadRequestException("Missing value of argument '\${$param->name}' in method '{$this->controllerName}::{$method}()'");
						}
						//dump($param->getType());
						$paramType                = $param->getType()?->getName();
						$actionArgs[$param->name] = $routerArgs[$param->name];
						if ($paramType !== null) {
							settype($actionArgs[$param->name], $paramType);
						}
					} else {
						if ( ! $param->isOptional()) {
							throw new BadRequestException("Missing argument '\${$param->name}' in method '{$this->controllerName}::{$method}()'");
						}
					}
				}
				$this->controller->{$method}(... array_values($actionArgs));
			} elseif ($this->httpRequest->getMethod() === 'POST'){
				$requestPost = $this->requestPost;
				$requestPost = array_map(function ($value) {
					if(!is_array($value)) {
						$value = trim($value);
						if ($value === "") {
							return null;
						}
					}
					return $value;
				}, $requestPost);
				
				
				$postData = ArrayHash::from($requestPost + $this->requestFiles);
				unset($postData->action);
				$this->controller->{$method}($postData);

			}
			//dumpe($this);
			//$this->controller->beforeRender();
			$this->controller->sendResponse();
			$alreadyServed = true;
		}

		$action = $this->httpRequest->getQuery('action') ?? $this->httpRequest->getPost('action') ?? ''; // $_REQUEST['action'] ?? '';
		$function = 'action_' . $action;
		if(function_exists($function) && !$alreadyServed){
			$reflection = new \ReflectionFunction($function);
			//$reflection = new \ReflectionFunction('myfunction');
			$filename = $reflection->getFileName();
			$start_line = $reflection->getStartLine() - 1; // it's actually - 1, otherwise you wont get the function() block
			$end_line = $reflection->getEndLine();
			$length = $end_line - $start_line;

			$source = file($filename);
			$body = implode("", array_slice($source, $start_line, $length));

			$isInline = !Strings::contains($body,"Content::Display()") || (isset($this->requestArguments["iframe"]) && $this->requestArguments["iframe"]) || $this->httpRequest->isAjax();

			//dump(Strings::contains($body,"Content::Display()"), $function);
			ob_start();
			call_user_func($function, $this->requestArguments);
			$html = ob_get_clean();
			//bdump($html);

			if(!$isInline) {
				$controller = $this->container->getByType(DefaultController::class);
				$controller->setAction('default');
				$controller->setUser($this->user);
				$controller->setUserService($this->userService);
				$controller->sendResponse();
			} else {
				echo $html;
			}


			$alreadyServed = true;
		}
		//dumpe($this);

		if(!$alreadyServed && !($this->controller instanceof ManagerAction)){
			$this->managerAction->setAction($this->action);
			$this->managerAction->setUser($this->user);
			$this->managerAction->setUserService($this->userService);

			$this->managerActionCheck[] = $this->controllerName;
			$this->managerActionCheck[] = $this->managerAction::class;

			$this->controller = $this->managerAction;
			$this->controllerName = $this->managerAction::class;

			$this->processRequest();
		}

		if(!$alreadyServed){
			throw new BadRequestException('Controller '. $this->formatErrorInController() .' with action \''.Route::presenter2path($this->action).'\' or function \''.$function.'\' not exists.');
		}
	}

	private function exposeBigFiles() : void
	{
		if($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST) && empty($_FILES) && $_SERVER['CONTENT_LENGTH'] > 0){
			die("WoW your file is too big!");
		}
	}

	private function checkCssFiles():void
	{
		$files = [/*"main.css"/*,"print.css"*/];
		$dir = APP_DIR."manager/";
		$moveDir = $dir."assets/css/";
		foreach($files as $file){
			//dump(basename($file));

			//dump($dir.$file,file_exists($dir.$file),file_exists($moveDir.$file));
			if(file_exists($dir.$file) && !file_exists($moveDir.basename($file))){
				FileSystem::copy($dir.$file, $moveDir.basename($file));
				$content = file_get_contents($moveDir.basename($file));
				if(Strings::contains($content, "url(img/")){
					$content = str_replace(["url(img/"],["url(../../img/"],$content);
					FileSystem::write($moveDir.basename($file), $content);
				}
			}
		}
		//dumpe($this);
	}
	private function checkJsFiles() : void {
		$file = APP_DIR."manager/js/init.js";
		$content = file_get_contents($file);
		if(Strings::contains($content, "\"#ribbon .r_top\"")){
			$content = str_replace(['"#ribbon .r_top"', '"#logobutton"','"#ribbon .r_bar"'], ['".r_top"','"#logobutton, .commerce-tab"','".r_bar"'], $content);
			FileSystem::write($file, $content);
		}
	}

	public function initAuth(): void
	{

		if(!$this->getUser()->isLoggedIn()){

			$this->controllerName = SignController::class;
			$this->controllerShortName = (new \ReflectionClass($this->controllerName))->getShortName();
			$this->action = in_array($this->action, ["default", "login"]) ? $this->action : 'default';
			$this->controller = $this->getController();

		} else {
			$Identity = $this->getUser()->getIdentity();
			Auth::$User = new \User($this->getUser()->getId());
			Auth::$auth=1;
			Auth::$userId=Auth::$User->id;
			Auth::$userName=Auth::$User->username;
			Auth::$mainPage=Auth::$User->mainpage;
			Auth::$identity = $Identity;
		}

		if(isset($this->requestArguments["logout"])){
			$this->userService->eraseCookie(UserService::NAMESPACE_ADMIN);
			$this->getUser()->logout();
			$this->httpResponse->redirect($this->httpRequest->getUrl()->getBasePath());
		}
	}

	protected function getControllerName() : ?string
	{
		$pathinfo = pathinfo($this->scriptFilename);

		$basename = str_replace(["mod.","."], ['',"-"], $pathinfo["filename"]);

		if(in_array($basename, ["index","manager","rpc"])){
			$basename = 'default';
		}
		$this->controllerShortName = Route::path2presenter($basename).'Controller';
		return static::CONTROLLER_NAMESPACE . $this->controllerShortName;

	}

	protected function getController(): ?ManagerController
	{
		//dumpe($this->container->findByTag($this->controllerShortName));
		if(!empty($serviceName = $this->container->findByTag($this->controllerShortName)/*$this->container->findByType($this->controllerName)*/)) {
			/** @var ManagerController $controller */
			$controller = $this->container->getService(key($serviceName));
			$controller->setUser($this->user);
			$controller->setUserService($this->userService);
			$controller->setAction($this->action);

			$this->controllerName = $controller::class;

			return $controller;
		}
		return null;
	}

	protected function formatErrorInController() {
		return "'".implode("', '", $this->managerActionCheck)."'";
	}

	protected function showLogin() {

		//include APP_DIR.'manager/login.php';
	}

	public function setErrorReporting(int $errorReporting): static
	{
		$this->errorReporting = $errorReporting;
		return $this;
	}

	public function getUser(): User
	{
		return $this->user;
	}

	public function getUserService(): UserService
	{
		return $this->userService;
	}

	public function getJs(bool $minify = false) : array {
		$files = [
			__DIR__ . "/assets/js/manager.js",
			//__DIR__ . "/assets/css/manager.css",
			//APP_DIR . "vendor/kubomikita/commerce-models/assets/css/common-manager.css"
		];
		return $this->createAssets($files, $minify);
	}

	private function createAssetFile(string $sourceFile, bool $minify = false, string $destinationDir = "assets/") : ?string
	{
		if(file_exists($sourceFile)){
			$pathinfo = pathinfo($sourceFile);
			$destinationDir .= $pathinfo["extension"] . "/";
			if($minify) {
				$jsmin = $pathinfo["extension"] === "css" ? new CSS($sourceFile) : new JS($sourceFile);
				$min   = $jsmin->minify();//static::$js_file
			} else {
				$min = file_get_contents($sourceFile);
			}

			FileSystem::createDir($destinationDir);
			$cssFile = $destinationDir . "_".$pathinfo["filename"].".min."/*.md5($min)."."*/.$pathinfo["extension"];
			$checksum = md5_file($cssFile);
			if($checksum !== md5($min)){
				FileSystem::write($cssFile,$min);
			}
			return $cssFile."?checksum=".$checksum;
		}
		//bdump($sourceFile);
		return null;
	}

	private function createAssets(array $sourceFiles, bool $minify = false) : array
	{
		$created = [];
		foreach ($sourceFiles as $file){
			if(($createdFile = $this->createAssetFile($file, $minify)) !== null){
				$created[] = $createdFile;
			}
		}
		return $created;
	}

	public function getLoginCss(bool $minify = false) : array
	{
		$files = [
			__DIR__ . "/assets/css/login.css"
		];
		return $this->createAssets($files, $minify);
	}



	public function getPrintCss(bool $minify = false):string
	{
		return $this->createAssetFile(__DIR__ . "/assets/css/print.css", $minify);
	}

	public function getCss(bool $minify = false) : array
	{
		$files = [
			__DIR__ . "/assets/css/main.css",
			__DIR__ . "/assets/css/manager.css",
			APP_DIR . "vendor/kubomikita/commerce-models/assets/css/common-manager.css"
		];
		return $this->createAssets($files, $minify);
	}

	public function getWarnings() : array
	{
		$warnings = [];
		$activeCron = (new \CronModel)->findAll()->where(["enabled" => 1, "day" => "*", 'weekday' => '*', "last <=" => (new DateTime())->modifyClone("-25 hour")])->count("id");
		if($activeCron > 0){
			$warnings[] = '<strong>Plánovač úloh</strong> (cron) nebol už 24 h spustený. Prosím kontaktujte správcu.';
		}

		return $warnings;
	}

	public function formatName(bool $withVersion = true) : string
	{
		return static::NAME . ($withVersion ? sprintf(" v%.3f", static::VERSION) : '');
	}

	public function releaseDate(): ?\DateTimeInterface{
		$githead = __DIR__."/../.git/refs/heads/master";
		if(file_exists($githead)){
			$dateTime = DateTime::from(filemtime($githead));
			return $dateTime;
		}

		return null;
		//if()
	}


	public function setRedirectWww(bool $redirectWww): void
	{
		$this->redirectWww = $redirectWww;
	}

	public function isRedirectWww(): bool
	{
		return $this->redirectWww;
	}
}
