<?php

namespace Commerce\Manager\Controller;

final class ImgselController extends ManagerController {

	public function actionDefault(){

		$folderId = $this->httpRequest->getQuery("folder") ?? 1;

		$FOLDER = new \ImageFolder($folderId);

		if($_REQUEST['CKEditor']!=''){ $_SESSION['cksel']['CKEditor']=$_REQUEST['CKEditor']; };
		if($_REQUEST['CKEditorFuncNum']!=''){ $_SESSION['cksel']['CKEditorFuncNum']=$_REQUEST['CKEditorFuncNum']; };
		if($_REQUEST['langCode']!=''){ $_SESSION['cksel']['langCode']=$_REQUEST['langCode']; };


		if($_REQUEST['a']=='del'){
			$F=new \ImageFolder($_REQUEST['folder']);
			$F->delete();

			$this->redirect("imgsel.php?folder=".$F->parentId);
		};

		if($_REQUEST['a']=='imgdel'){
			$I=new \StaticImageLink($_REQUEST['id']);
			$I->delete();
			header("Location: imgsel.php?folder=".$I->folderId);
			exit();
		};

		if($_REQUEST['a']=='newfolder'){
			$F=new \ImageFolder();
			$F->nazov=$_REQUEST['nazov'];
			$F->parentId=$_REQUEST['folder'];
			$F->save();
			header("Location: imgsel.php?folder=".$F->parentId);
			exit();
		};

		if($_REQUEST['a']=='renamefolder'){
			$F=new \ImageFolder($_REQUEST['folder']);
			$F->nazov=$_REQUEST['nazov'];
			$F->save();
			header("Location: imgsel.php?folder=".$F->parentId);
			exit();
		};

		if($_REQUEST['a']=='upload'){
			$D=new \UploadedFile('imagefile');
			$I=new \StaticImageLink();
			$I->filename=$D->name;
			$I->content=$D->content;
			$I->folderId=$_REQUEST['folder'];
			$I->save();
			$I->saveData();
			header("Location: imgsel.php?folder=".$I->folderId."&return=".$I->id);
			exit();
		};

		$this->template->FOLDER = $FOLDER;
		if(($return = $this->httpRequest->getQuery("return")) !== null) {
			$I=new \StaticImageLink($return);
			$this->template->return = $I;
		}
	}
}
