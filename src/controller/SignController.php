<?php

namespace Commerce\Manager\Controller;

use Kubomikita\Flash;
use Nette\Utils\ArrayHash;

final class SignController extends ManagerController {

	public function actionDefault(){
		//$this->te
		bdump(func_get_args());
	}
	public function actionLogin(ArrayHash $data){

		try {
			if($this->httpRequest->getPost("logged") !== null){
				$this->user->setExpiration(null);
			} else {
				$this->user->setExpiration('15 minutes');
			}
			$this->user->login($this->httpRequest->getPost("username"), $this->httpRequest->getPost("password"));
			if($this->httpRequest->getPost("logged") !== null){
				$this->userService->createCookie(\Kubomikita\Commerce\UserService::NAMESPACE_ADMIN, $this->user->getIdentity()->getId());
			}
		} catch (\Nette\Security\AuthenticationException $e) {
			Flash::danger($e->getMessage());
		}

		$this->redirect($this->httpRequest->getReferer());
	}

}