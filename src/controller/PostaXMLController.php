<?php

namespace Commerce\Manager\Controller;

use Commerce\Manager\Utils\ExportPostaXml;

final class PostaXMLController extends ManagerController {

	public function actionDefault(){
		//dumpe($this);
		if(isset($_GET["delete"]) and is_numeric($_GET["delete"])){
			$obj=new ExportPostaXml($_GET["delete"]);
			$ids=$obj->selectSameXml();
			$xml=array_shift($ids);
			$xml_file=array_shift($ids);
			//var_dump($xml,$xml_file);
			$json=array();
			foreach($ids as $id){
				$json[$id]=$id;
			}
			$json=json_encode($json);
			$obj->deleteFromDB($xml, $xml_file);
			if(file_exists($xml_file)) unlink($xml_file);
			//var_dump($ids);
			//var_dump($json);
			if($xml==1){
				header("location: postaXML.php?balik=$json");exit;
			} elseif($xml==2){
				header("location: postaXML.php?list=$json");exit;
			}
			exit;
		}
		if(isset($_GET["balik"])){
			$baliky=(array)json_decode($_GET["balik"]); $druhZasielky["balik"]=4;

			$datum=date("Ymd");
			$pocetBalikov=count($baliky);
			$zasielky=<<<EOT
    <EPH verzia='3.0' xmlns='http://ekp.posta.sk/LOGIS/Formulare/Podaj_v03'>
        <InfoEPH>
            <Mena>EUR</Mena>
            <TypEPH>1</TypEPH>
            <EPHID></EPHID>
            <Datum>$datum</Datum>
            <PocetZasielok>$pocetBalikov</PocetZasielok>
            <Uhrada>
                <SposobUhrady>8</SposobUhrady>
                <SumaUhrady></SumaUhrady>
            </Uhrada>
            <DruhZasielky>14</DruhZasielky>
            <Odosielatel>
                <OdosielatelID>WEB_EPH</OdosielatelID>
                <Meno>WWW.PROTEIN.SK</Meno>
                <Organizacia>MAJER POD LESOM s.r.o.</Organizacia>
                <Ulica>Pod lesom 1346</Ulica>
                <Mesto>Ľubica</Mesto>
                <PSC>05 971</PSC>
                <Krajina>SK</Krajina>
                <Telefon>0914 777 111</Telefon>
                <Email>sekretarka@protein.sk</Email>
                <CisloUctu>2924901916/1100</CisloUctu>
                
            </Odosielatel>
        </InfoEPH>

EOT;


			$zasielky.="<Zasielky>";
			foreach($baliky as $obj_id => $val){
				$obj=new ExportPostaXml($obj_id);

				if($obj->info){
					$kod=$obj->getPodacieCislo();
					//dump($kod);
					$meta = unserialize($obj->info["metadata"]);
					$sluzby="";
					if(isset($meta["nedorucovat"]) && $meta["nedorucovat"] == true){
						$sluzby = <<<EOT
                    <PouziteSluzby>
                        <Sluzba>PR</Sluzba>
                    </PouziteSluzby>

EOT;
					}
					//var_dump($obj->getPlatba());
					//var_dump($obj->getAdresa());
					$klient=$obj->getAdresa();
					$cislo_obj=$obj->info["cislo"];
					if($obj->getPlatba()=="dobierka"){ $cenaDobierky="<CenaDobierky>".$obj->info["_cache_suma"]."</CenaDobierky>";$druhPPP="<DruhPPP>1</DruhPPP>";} else {$cenaDobierky="";$druhPPP="";}
					$client = trim($klient["titul"]." ".$klient["meno"]." ".$klient["priezvisko"]);
					$zasielky.= <<<EOT
                <Zasielka>
                    <Adresat>
                        <Meno>$client</Meno>
                        <Ulica>$klient[adresa]</Ulica>
                        <Mesto>$klient[mesto]</Mesto>
                        <PSC>$klient[psc]</PSC>
                        <Krajina>SK</Krajina>
                        <Telefon>$klient[telefon]</Telefon>
                        <Email>$klient[email]</Email>
                    </Adresat>
                    <Info>
                        <Hmotnost>{$obj->getHmotnost()}</Hmotnost>
                        $cenaDobierky
                        <CenaVyplatneho></CenaVyplatneho>
                        <CenaPoistneho>30</CenaPoistneho>
                        <Trieda>1</Trieda>
                        $druhPPP
                        <CiarovyKod>$kod[full]</CiarovyKod>
                        <SymbolPrevodu>$cislo_obj</SymbolPrevodu>
                    </Info>
                    $sluzby
                </Zasielka>
EOT;
					//echo "<hr>";
					$obj->updateXML(1,$kod);
				}
			}
			if($obj->info){
				$zasielky.="</Zasielky></EPH>";
				$nazovXML="balik_".date("Ymd_H-i-s");
				if(!file_exists(APP_DIR."manager/temp")){
					mkdir(APP_DIR."manager/temp");
				}
				file_put_contents(APP_DIR."manager/temp/$nazovXML.xml", $zasielky);
				//echo "\n\n\n\n<a href=\"temp/balik_$nazovXML.xml\" target=\"_blank\">temp/balik_$nazovXML.xml</a>";
				foreach($baliky as $id => $val){
					$obj->update("UPDATE ec_objednavky SET xml_file='temp/$nazovXML.xml' WHERE id='$id'");
				}
			}
			header("location: $_SERVER[HTTP_REFERER]");
			exit;
		}
	}
}
