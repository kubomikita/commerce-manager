<?php
namespace Commerce\Manager\Controller;


use Auth;
use CmsGalleryImgModel;
use CmsGalleryModel;
use Content;
use Nette\Application\ForbiddenRequestException;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use UploadedFile;

class RsImagesController extends  ManagerController
{
	public function startup(): void
	{
		parent::startup();
		if(!Auth::ACL('stock_edit')){
			throw new ForbiddenRequestException();
		}
	}
	function actionDefault()
	{
		$this->setTitle("Fotogalérie","img_images.gif","img/rse/");

		$this->template->q = $this->db->query("SELECT * FROM cms_img_galerie");
	}

	function actionEdit(int $id){
		$r = new CmsGalleryModel($id);

		$this->setTitle("Úprava galérie: ".$r->nazov,"img_images.gif","img/rse/");

		$this->addTab("default", "Všeobecné");
		$this->addTab("images", "Obrázky");

		$this->addLeftButton("submit", "Zapísať zmeny","work_start(); $('#edit_form').submit();");
		$this->addLeftButton("return", "Návrat", "location.href='mod.rs.images.php';");

		$this->template->r = $r;
		$this->template->acl = Auth::ACL('stock_edit');
	}

	function actionDelete(int $id){

		$r=new CmsGalleryModel($id);
		$r->delete();
		$this->flashMessage("Galéria bola vymazaná.", \Flash::TYPE_DANGER);
		$this->redirect("mod.rs.images.php");
	}
	function actionSave(ArrayHash $data){

		$id = $data["id"];
		//dump($data);
		$r = new \CmsGalleryModel($id);
		$r->nazov = $data["nazov"];
		$r->popis = $data["popis"];
		$r->save();
		$this->flashMessage("Galéria bola upravená.");
		$this->redirect("mod.rs.images.php?action=edit&id=".$id);

	}

	function actionImagesUpload($X){
		$T=new CmsGalleryModel($X['t_id']);

		$D=new UploadedFile('imagefile');
		$I=new CmsGalleryImgModel();
		$I->predajnaId=$T->id;
		$I->content=$D->content;
		$I->save();
		$I->saveData();
		\LogAction::add("[user] pridal obrázky k galerii [objectId].",null,null,$T->nazov());
		//header("Location: mod.rs.images.php?action=images_view&t_id=".$T->id);
		$this->redirect("mod.rs.images.php?action=edit&id=".$T->id."&tab=images");
	}
	function actionImagesDelete(int $id){
		$I=new CmsGalleryImgModel($id);
		\LogAction::add("[user] vymazal obrázok k galerii [objectId].",null,null,$I->predajnaId);
		$I->delete();
		$this->redirect("mod.rs.images.php?action=edit&id=".$I->predajnaId."&tab=images");	}

	function actionImagesMoveUp(int $id){
		$I=new CmsGalleryImgModel($id);
		$I->moveUp();
		$this->redirect("mod.rs.images.php?action=edit&id=".$I->predajnaId."&tab=images");	}

	function actionImagesMoveDown(int $id){
		$I=new CmsGalleryImgModel($id);
		$I->moveDown();
		$this->redirect("mod.rs.images.php?action=edit&id=".$I->predajnaId."&tab=images");	}

	function actionImagesMoveTop(int $id){
		$I=new CmsGalleryImgModel($id);
		$I->moveTop();
		$this->redirect("mod.rs.images.php?action=edit&id=".$I->predajnaId."&tab=images");	}

	function actionImagesMoveBottom(int $id){
		$I=new CmsGalleryImgModel($id);
		$I->moveBottom();
		$this->redirect("mod.rs.images.php?action=edit&id=".$I->predajnaId."&tab=images");
	}

	public function actionImageEdit(int $id) {
		$i = new CmsGalleryImgModel($id);
		$this->setTitle("Úprava obrázka: ".$i->id,"img_images.gif","img/rse/");
		$this->addLargeButton("submit","Uložiť","$('#edit_form').submit();");
		$this->addLargeButton("return","Späť","location.href='mod.rs.images.php?action=edit&id={$i->predajnaId}&tab=images'");
		$this->template->i = $i;

	}

	function actionImageSave($X){
		$i = new CmsGalleryImgModel($X["id"]);
		$i->meno = $X["nazov"];
		$i->odkaz = $X["odkaz"];
		$i->popis = $X["popis"];
		$i->podnadpis = $X["podnadpis"];
		$i->save();
		//header("Location: mod.rs.images.php?action=images_view&t_id={$i->predajnaId}" );
		$this->flashMessage("Obrázok bol upravený.");
		$this->redirect("mod.rs.images.php?action=image_edit&id=".$i->id."");
	}
}