<?php

namespace Commerce\Manager\Controller;

use CmsHtml;
use Content;
use Nette\Http\FileUpload;
use Nette\Utils\ArrayHash;

class RsContentController extends ManagerController
{
	public function actionDefault(){
		$this->setTitle("Texty", "list");
		$this->template->q= CmsHtml::fetchAll();
		$this->addLargeButton("add","Pridať obsah", "location.href='mod.rs.content.php?action=add'");
	}

	function actionEdit(int $id){

		$r = new CmsHtml($id);
		$this->setTitle("Úprava textu: ".$r->nazov, "list");
		$this->addLargeButton('submit', 'Uložiť', "$('#edit_form').submit();");
		$this->addLargeButton("return","Späť","location.href='mod.rs.content.php'");

		$this->template->r = $r;
		$this->template->q= CmsHtml::fetchAll();
	}

	function actionDelete(int $id){
		$c = new CmsHtml($id);
		$c->delete();
		$this->redirect("mod.rs.content.php");
	}

	function actionAdd(){
		$this->setTitle("Nový obsah","list");
		$p = new CmsHtml();
		$p->nazov = "Nový obsah";
		$p->content = '<p>Obsah pripravujeme...</p>';

		$this->template->r = $p;

		$this->addLargeButton('submit', 'Uložiť', "$('#edit_form').submit();");
		$this->addLargeButton("return","Späť","location.href='mod.rs.content.php'");
	}


	function actionSave(ArrayHash $data){

		$id = $data->id;
		unset($data["id"]);
		unset($data["action"]);

		bdump($data);
		$c = new CmsHtml($id);
		$c->save((array) $data);

		$this->redirect("mod.rs.content.php?action=edit&id=".$c->id);

	}
}