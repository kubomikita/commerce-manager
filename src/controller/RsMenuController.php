<?php
namespace Commerce\Manager\Controller;

use CmsMenu;
use Content;
use Nette\Utils\ArrayHash;

class RsMenuController extends  ManagerController{
	function actionDefault(){
		$this->setTitle("Menu", "clipboard");

		$this->template->q = CmsMenu::fetchAll();


	}
	function actionEdit(int $id){

		$r=$this->db->query("SELECT * FROM cms_menu WHERE id=?",$id)->fetch();
		$this->setTitle("Úprava menu: ". $r->nazov, "clipboard");

		$this->template->r = $r;
		$this->template->data = unserialize($r->xdata);

		$this->addLargeButton('submit', 'Uložiť', "$('#edit_form').submit();");
		$this->addLargeButton("return","Späť","location.href='mod.rs.menu.php'");

	}
	function actionSave(ArrayHash $values){
		$data = $values;
		$xdata = [];

		foreach($data["text"] as $k=>$v){
			if(strlen(trim($v)) >0){
				$xxdata["group"] = $data["group"][$k];
				$xxdata["text"] = $data["text"][$k];
				$xxdata["link"] = $data["link"][$k];
				$xxdata["class"] = $data["class"][$k];
				$xxdata["sub"] = $data["sub"][$k];
				$xdata[] = $xxdata;
			}
		}

		$id = $values["id"];
		unset($data["id"]);
		unset($data["action"]);
		unset($data["group"]);
		unset($data["link"]);
		unset($data["text"]);
		unset($data["class"]);
		unset($data["sub"]);
		$data["xdata"] = ($xdata);


		$o = new CmsMenu($id);
		$o->save((array) $data);
		$this->flashMessage("Menu bolo uložené.");
		$this->redirect("mod.rs.menu.php?action=edit&id=".$id);

	}

}