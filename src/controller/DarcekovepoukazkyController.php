<?php


namespace Commerce\Manager\Controller;

use Kubomikita\Flash;
use Nette\Application\ForbiddenRequestException;
use Nette\Database\Explorer;
use Nette\Http\Url;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;
use Nette\Utils\Random;
use Nette\Utils\Strings;
use Auth;
use Voucher;


class DarcekovepoukazkyController extends ManagerController
{

	public function startup(): void
	{
		parent::startup();
		if(! Auth::ACL('stock_view')){
			throw new ForbiddenRequestException();
		}
	}

	public function actionDefault(){
		$this->setTitle("Darčekové poukážky", "promo");
		$this->addLeftButton('add','Pridať kód', "location.href='mod.darcekovepoukazky.php?action=edit&id=0';");
		$this->addLeftButton('batch','Generátor kódov', "location.href='mod.darcekovepoukazky.php?action=generate';");
		$this->addLeftButton('sheet',"Stiahnúť CSV aktívne", "location.href='mod.darcekovepoukazky.php?action=toCsv';");
		$this->template->list = Voucher::fetch();
	}

	public function actionEdit(int $id){
		$PK=new Voucher($id);
		if($id == 0){
			$PK->kod = /*Strings::upper(Random::generate(15)); */ strtoupper(substr(sha1(time().uniqid()),15,15));
			$PK->platnost_do = (new \Timestamp((new DateTime())->modifyClone("+1 year")->format("Y-m-d")));
		}

		$this->template->PK = $PK;
		$this->template->shopUrl = $this->container->parameters["shop"]["url"];
		$this->template->url = $this->template->shopUrl.'api/voucher/get?type=full&value='.$PK->suma.'&code='.$PK->kod.'&valid='.$PK->platnost_do->show('d.m.Y');
		$this->template->voucher_id = $id;
		//bdump($this->template);

		$this->setTitle("Darčeková poukážka - ".($id?LangStr('ÚPRAVA'):LangStr('NOVÁ')), "promo");
		$this->addLeftButton("submit","Zapísať zmeny","$('#mainform').submit();");
		if($PK->id !== null && $PK->deletable()){
			$this->addLeftButtonConfirm("delete","Zmazať","location.href='mod.darcekovepoukazky.php?action=delete&id=".$PK->id."';");
		}
		$this->addLeftButton("return","Návrat","location.href='mod.darcekovepoukazky.php';");
	}

	public function actionSave(ArrayHash $X) {

		if($X['id']){ $PK=new Voucher($X['id']); } else { $PK=new Voucher(); 	};

		$PK->kod=$X['kod'];
		$PK->popis=$X['popis'];
		//$PK->skratka=$X['skratka'];
		$PK->platnost_od=Timestamp($X['platnost_od'])->dayStart();
		$PK->platnost_do=Timestamp($X['platnost_do'])->dayEnd();
		$PK->aktivny=(int)$X['aktivny'];
		$PK->suma=(float)$X['suma'];
		$PK->suma_od=(float)$X['suma_od'];
		$PK->save();

		if($X["email"] != ""){
			$Y = array();
			$Y["email"] = $X["email"];
			$Y["id"] = $PK->id;
			//action_send_to_email($Y);
		}
		$this->flashMessage("Darčeková poukážka bola uložená.");
		$this->redirect("mod.darcekovepoukazky.php");
	}
	public function actionDelete(int $id){

		if($id){
			$PK=new Voucher($id);
			$PK->delete();
		};
		$this->flashMessage("Darčeková poukážka bola vymazaná.", Flash::TYPE_DANGER);
		$this->redirect("mod.darcekovepoukazky.php");
	}

	public function actionGenerate() {

		$this->setTitle("Generátor kódov", "batch");
		$this->addLeftButton("submit","Vygenerovať","$('#mainform').submit();");
		$this->addLeftButton("return","Návrat","location.href='mod.darcekovepoukazky.php';");

		$this->template->platnost = (new DateTime())->modify("+1 year");

	}
	public function actionGenerateSave(ArrayHash $X){
		//dump($X);

		$generated = [];
		for($i=0;$i<(int) $X["pocet"];$i++){
			$prefixLenght = strlen($X["prefix"]) + 1;
			$lenght = 15 - $prefixLenght;
			$row = [
				"kod" => strtoupper($X["prefix"].'-'.substr(sha1(time().uniqid()),15,$lenght)),
				"platnost_od" => (new DateTime())->modify("midnight")->getTimestamp(),
				"platnost_do" => (new DateTime($X['platnost_do']))->modify("today 23:59")->getTimestamp(), //Timestamp($X['platnost_do'])->dayEnd(),
				"aktivny" => 1,
				"suma" => $X["suma"],
				"suma_od" => $X["suma_od"],
			];

			$generated[] = $row;
		}

		if(!empty($generated)) {
			$this->container->getByType(Explorer::class)->table("ec_darcekove_poukazky")->insert($generated);
		}

		\Cache::flush(Voucher::getCacheGroup());

		$this->flashMessage("Darčekové poukážky boli vygenerované.", Flash::TYPE_DANGER);
		$this->redirect("mod.darcekovepoukazky.php");
	}

	public function actionToCsv() {
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename="dp.csv"');
		$separator = ';';
		$fp = fopen('php://output', 'wb');
		fputcsv($fp, ['kod', 'platnost_od', 'platnost_do',"suma", "suma_od"], $separator);

		foreach ( Voucher::fetch() as $dp ) {
			if($dp->aktivny()) {
				fputcsv($fp, [
					$dp->kod,
					$dp->platnost_od->show('d.m.Y'),
					$dp->platnost_do->show('d.m.Y'),
					$dp->suma,
					$dp->suma_od
				], $separator);
			}
		}
		fclose($fp);

		exit;
	}
}