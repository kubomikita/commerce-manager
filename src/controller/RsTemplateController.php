<?php
namespace Commerce\Manager\Controller;


use Auth;
use Cache;
use CmsGalleryImgModel;
use CmsGalleryModel;
use Commerce\ControllerFactory;
use Content;
use Nette\Application\ForbiddenRequestException;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Nette\Utils\Strings;
use PageData;
use Template;
use TemplateData;
use UI;
use UploadedFile;

class RsTemplateController extends  ManagerController
{
	public function startup(): void
	{
		parent::startup();
		if(!Auth::ACL('business_view')){
			throw new ForbiddenRequestException();
		};
	}

	public function actionDefault(){

		$this->setTitle("Zoznam šablón","img_template.gif");

		foreach (Template::fetch([], "id ASC") as $template){
			$this->addTab("template_".$template->id, $template->nazov,true/*,  ["template" => $template]*/);
		}

		//dump(CommerceDB::$DB);//,EchelonDB::DB());
		//dump(Template::fetch());
		/*$system_pages = Template::fetch([],"id ASC");
		$out='<div class="in"><table class="list">';
		$out.='<tr><th>id</th><th>Názov</th><th>Šablona</th>';
		$out.='<th>&nbsp;</th></tr>';
		foreach($system_pages as $p){
			$rowClass=array('listrow','activerow');
			if($p->main){
				$rowClass[] = "highlightedrow";
			}
			//dump($p);
			$out.='<tr ref="mod.rs.template.php?action=edit&id='.$p->id.'" class="'.implode(" ",$rowClass).'">';
			$out.='<td>'.$p->id.'</td>';
			$out.='<td><div><strong>'.$p->nazov.'</strong></div> <small>'.$p->popis.'</small></td>';
			$out.='<td width="200">'.$p->src.'</td>';
			$out.='<td width="50"><a  href="mod.rs.template.php?action=edit&id='.$p->id.'"><img src="img/16/follow.png"></a></td>';
			$out.= '</tr>';
		}
		$out.='</table></div>';

		Content::$Mainbox.=$out;*/

		//Content::Display();
	}
	public function actionTemplate(int $id){
		$p = new Template($id);
		$this->template->template = $this->template->p = $p;

		$boxes = $boxesidsToName=  [];
		foreach($p->data() as $data){
			$boxes[$data->box][] = $data;
		}
		foreach($p->boxes() as $ident => $B){
			$boxesidsToName[$ident] = $B["info"];
			if(!isset($boxes[$ident])){
				$boxes[$ident][] = new TemplateData();
			}
			//
		}



		ksort($boxes);


		$this->template->boxes = $boxes;
		$this->template->boxesidsToName = $boxesidsToName;
		$this->template->OBJ = $this->container->getByType(ControllerFactory::class);
		$this->template->boxesIdentToName = $p->getBoxesName();

		$this->setTitle("Šablóna - ".$p->nazov, "img_template.gif");


	}

	function actionAddObject(int $template_id, int $box)
	{
		$p                   = new Template($template_id);
		$this->template->p   = $p;
		$this->template->box = $box;

		$this->setTitle("Šablóna - " . $p->nazov . " - pridanie objektu do boxu č. " . $box, "img_template");
		$this->addLargeButton('submit', 'Uložiť', "$('#edit_form').submit();");
		$this->addLargeButton("return", "Späť", "location.href='mod.rs.template.php?tab=template_".$template_id."#box_{$box}'");
		$this->template->vals = [];
		foreach ($this->context->getByType(ControllerFactory::class) as $k => $v) {
			$this->template->vals[$k] = '<img src="img/rse/' . $v->objIcon . '"> ' . $v->objName;
		}

	}

	public function actionSaveObject(ArrayHash $data){

		if(empty($data->object)){
			$this->flashMessage("Vyberte aspon jeden objekt", \Flash::TYPE_DANGER);
			$this->redirect("mod.rs.template.php?action=add_object&template_id=".$data->template_id."&box=".$data->box);
		}
		$q=$this->db->query("SELECT max(poradie) as newporadie FROM cms_templates_data WHERE template = ? and box = ?",$data["template_id"],$data["box"])->fetch();
		$newporadie = $q["newporadie"] + 1;

		foreach ($data->object as $object){
			$this->db->query("INSERT INTO cms_templates_data",["template"=>$data["template_id"],"box"=>$data["box"],"object"=>$object,"poradie"=>$newporadie,"show"=>"hide"]);
			$newporadie++;
		}

		Cache::flush("cms.templates_data");
		Cache::flush("cms.templates");
		$this->flashMessage("Objekt bol pridaný.");
		$this->redirect("mod.rs.template.php?tab=template_".$data->template_id."#box_".$data["box"]);
	}

	function actionPagedataedit(string $class, int $id, $uri, $uri_id = null){
		$OBJ = $this->container->getByType(\Commerce\ControllerFactory::class); //CommerceController::startup();
		$action = "pagedatasave";
		if($class == "TemplateData"){
			$p = new TemplateData( $id);
			$action = "templatedatasave";
			$back = "mod.rs.template.php?#box_{$p->box}";
		} else {
			$p = new PageData( $id );
			$back = 'mod.rs.pages.php?action=edit&id='.$p->page;
		}
		$c = $OBJ[$p->object];

		$this->setTitle("Úprava objektu: ".$c->objName,$c->objImage, "img/rse/");

		$this->addLargeButton('submit', 'Uložiť', "$('#edit_form').submit();");
		$this->addLargeButton("return", "Späť", "location.href='{$back}'");
		$this->template->p = $p;
		$this->template->c = $c;
		$this->template->action = $action;


	}

	public function actionEditBox($boxid) {

		$r = $this->db->query("SELECT * FROM cms_templates_boxes WHERE ident = ?", $boxid)->fetch();

		$this->setTitle("Úprava boxu šablóny: ".$r->nazov,"img_template.gif");
		$this->template->r = $r;

		$this->addLargeButton('submit', 'Uložiť', "$('#edit_form').submit();");
		$this->addLargeButton("return", "Späť", "location.href='mod.rs.template.php#box_".$boxid."'");
	}

	public function actionSaveBox(ArrayHash $data){

		$id = $data->id;
		$ident = $data->ident;
		unset($data->id, $data->ident);
		$data->ident_text = strlen(trim($data->ident_text)) == 0 ? Strings::webalize($data->nazov) : Strings::webalize($data->ident_text);
		$data->boxid = Strings::webalize($data->boxid);

		$this->db->query("UPDATE cms_templates_boxes SET", (array) $data, "WHERE id = ?", $id);

		Cache::flush("cms.templates_data");
		Cache::flush("cms.templates");

		$this->flashMessage("Box bol uložený.");
		$this->redirect("mod.rs.template.php#box_".$ident);

	}
}