<?php

namespace Commerce\Manager\Controller;

use Auth;
use Content;
use Kubomikita\Commerce\Model\PscUlicaModel;
use Kubomikita\Flash;
use Nette\Application\ForbiddenRequestException;
use Nette\Utils\ArrayHash;

final class PscController extends ManagerController {
	public function startup(): void
	{
		parent::startup();
		if(! Auth::ACL('business_view')){
			throw new ForbiddenRequestException();
		}
	}

	public function actionDefault()
	{
		$this->setTitle("PSČ", "delivery.png");
		$zips = \PredajnaModel::getExpresDeliveryZips();
		$this->template->zips = \PscUlicaModel::getByPsc($zips);
		$this->template->missing = array_diff(array_keys($zips), array_keys($this->template->zips));
	}

	public function actionEdit(string $psc){
		$this->setTitle("Úprava PSČ: ".$psc, "delivery.png");
		$this->addLargeButton('submit', 'Uložiť', "$('#edit_form').submit();");
		$this->addLargeButton("return","Späť","location.href='mod.psc.php'");
		$this->template->psc = $psc;
		$pscModel = PscUlicaModel::fromPsc($psc);
		$this->template->nazov = $pscModel->nazov ?? null;

	}

	function actionSave(ArrayHash $data){

		$psc = $data->psc;
		unset($data["psc"]);
		unset($data["action"]);

		$data->nazov = strlen(trim($data->nazov)) > 0 ? $data->nazov : null;

		PscUlicaModel::updateByPsc($psc, (array) $data);

		Flash::success("Názov pre PSČ bol uložený.");
		$this->redirect("mod.psc.php?action=edit&psc=".$psc);

	}
	public function actionAdd($psc){
		$this->setTitle("Pridať PSČ", "delivery.png");
		$this->addLargeButton('submit', 'Uložiť', "$('#edit_form').submit();");
		$this->addLargeButton("return","Späť","location.href='mod.psc.php'");
		$this->template->psc = $psc;

	}

	public function actionSaveNew(ArrayHash $data){
		if(empty($data->nazov) || empty($data->ulica) || empty($data->obec)){
			Flash::danger("Všetky polia sú povinné.");
			$this->redirect("mod.psc.php?action=add&psc=".$data->psc);
		}

		$n = new PscUlicaModel();
		$n->psc = $data->psc;
		$n->nazov = $data->nazov;
		$n->obec = $data->obec;
		$n->ulica = $data->ulica;

		$n->save();

		Flash::success("PSČ bolo vytvorené.");

		$this->redirect("mod.psc.php");
	}
}