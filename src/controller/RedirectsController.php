<?php

namespace Commerce\Manager\Controller;

use Auth;
use Content;
use Kubomikita\Flash;
use Nette\Application\ForbiddenRequestException;
use Nette\Utils\ArrayHash;

final class RedirectsController extends ManagerController {

	public function startup(): void
	{
		parent::startup();
		if(! Auth::ACL('clients_edit')){
			throw new ForbiddenRequestException();
		}
	}



	public function actionDefault(){
		$this->setTitle("SEO presmerovania", "batch.png");
		$this->template->links = \LinkMapper::find("remap != '' ORDER BY  id DESC");
		//$this->addLeftButton('add','Pridať stav',"mod.objednavky.stavy.php?action=edit&id=0");
	}
	public function actionSave(ArrayHash $data){
		$n = new \LinkMapper;
		$n->link = $data["link"];
		$n->remap = $data["remap"];

		$n->status = $data["status"];;
		$n->save();
		$this->flashMessage('Presmerovanie bolo uložené.');
		$this->redirect("mod.redirects.php");
	}
	public function actionDelete(int $id){
		$n = new \LinkMapper($id);
		$n->delete();
		$this->flashMessage('Presmerovanie bolo vymazané.', \Flash::TYPE_DANGER);
		$this->redirect("mod.redirects.php");
	}
}