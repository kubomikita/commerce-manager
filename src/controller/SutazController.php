<?php

namespace Commerce\Manager\Controller;

use Kubomikita\Commerce\EcomailProvider;
use Kubomikita\Flash;
use Kubomikita\Form;
use Kubomikita\FormBlock;
use Latte\Engine;
use Nette\Application\BadRequestException;
use Nette\Application\ForbiddenRequestException;
use \Auth;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;
use Nette\Utils\Json;
use Nette\Utils\Validators;

class SutazController extends ManagerController {
	public function startup(): void
	{
		parent::startup();
		if(! Auth::ACL('business_view')){
			throw new ForbiddenRequestException();
		}
	}

	public function actionDefault() {
		$this->setTitle("Súťaž", "money_bag.png");
		//dumpe($this["winnersForm"]);
		//bdump($this->context->getByType(EcomailProvider::class)->unsubscribe("miki_88@azet.sk"));
		//bdump($this->context->getByType(EcomailProvider::class)->subscribe("miki_88@azet.sk"));
		$this->template->vyhercovia = \SutazVyhercoviaModel::fetchAll([], "date ASC");
	}

	public function actionDraw(int $id){
		$v = new \SutazVyhercoviaModel($id);
		if($v->id === null){
			throw new BadRequestException();
		}

		$vyhercovia = [];
		foreach ($v->getCupons() as $cupon){
			$vyhercovia[$cupon->id] = $cupon->meno ." ". $cupon->priezvisko ." | ".$cupon->email ." | ". $cupon->telefon ." | ".$cupon->kod;
		}
		bdump($vyhercovia);

		$this->template->v = $v;

		$defaults = (array) $v->row();
		unset($defaults["date_draw"]);
		$this["winnersForm"]->setDefaults($defaults);
		$this["winnersForm"]["sutaz"]->setItems($vyhercovia);
	}

	public function actionShow( int $id){
		$v = new \SutazVyhercoviaModel($id);
		if($v->id === null){
			throw new BadRequestException();
		}

		$v->show = !$v->show;
		$v->save();

		$this->redirect("mod.sutaz.php");
	}

	public function actionEmail(int $id){
		$v = new \SutazVyhercoviaModel($id);
		if($v->id === null){
			throw new BadRequestException();
		}
		if(($sutaz = $v->sutaz()) !== null){
			$latte = $this->context->getByType(Engine::class);

			$email = $sutaz->email;
			if($v->sendToEmail($email)){
				$this->flashMessage("E-mail bol odoslaný.", Flash::TYPE_SUCCESS);
			} else {
				$this->flashMessage("E-mail sa nepodarilo odoslať.", Flash::TYPE_DANGER);
			}
			/*$body = $latte->renderToString(MAIL_DIR."sutazVyhra.latte");

			if(Validators::isEmail($email)) {
				$M = new \Mail();
				$M->setSubject('Gratulujeme k výhre na ' . $this->context->getParameter("shop", "name"));
				$M->addTo($email);
				$M->setBody($body);
				$M->send();
				$v->save(["email" => 1, "show" => 1]);
				$this->flashMessage("E-mail bol odoslaný.", Flash::TYPE_SUCCESS);
			} else {
				$this->flashMessage("E-mail sa nepodarilo odoslať.", Flash::TYPE_DANGER);
			}*/
		}
		$this->redirect("mod.sutaz.php");

	}

	public function createComponentWinnersForm($name) : Form
	{
		$f = new Form($name);
		//$f->setClass("live");
		$f->setAjax();

		$vyhercovia = [];


		$b = new FormBlock($f);
		$b->setItemClass("form-control form-control-sm");
		$b->setGroupElement('div class="form-group"');
		$b->addHidden("id");
		$b->addText("date_draw","Dátum a čas žrebovania")->setValue((new DateTime())->format("d.m.Y H:i:s"))->setRequired();
		$b->addSelect("sutaz", "Výherca", $vyhercovia)->setPrompt("-- vyberte --")->setRequired();
		$b->addCheckbox("show", "Zobraziť v zozname");

		$b->addSubmit("save","Uložiť");
		//$b->addPlainHTML("test");

		$f->addBlock($b);



		$f->onSuccess[] = function (Form $form, ArrayHash $values){
			bdump($values, "FORM VALUES");

			$v = new \SutazVyhercoviaModel($values->id);
			if($v->id !== null){
				unset($values->id);
				$values->date_draw = new DateTime($values->date_draw);
				bdump($values);
				bdump($v);
				$v->save((array) $values);
			}
			$this->flashMessage("Výhra bola uložená.", Flash::TYPE_SUCCESS);
			$this->redirect("mod.sutaz.php");
		};

		return $f;
	}
}