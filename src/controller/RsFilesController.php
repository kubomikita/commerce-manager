<?php

namespace Commerce\Manager\Controller;

use Content;
use Nette\Http\FileUpload;
use Nette\Utils\ArrayHash;

class RsFilesController extends ManagerController
{
	public function actionDefault()
	{
		$this->setTitle("Zoznam súborov", "img_file.gif", 'img/rse/');
		$this->template->list = $this->db->query("SELECT * FROM cms_file_list");
	}




	public function actionEdit(int $id)
	{

		$this->template->rr   = $rr = $this->db->query("SELECT * FROM cms_file_list WHERE id=?", $id)->fetch();
		$this->template->list = $this->db->query("SELECT *,length(bdata) as fsize FROM cms_file_data WHERE container = ? ORDER BY poradie",
			$rr->id);
		$this->setTitle("Úprava súborov: " . $rr->nazov, "img_file.gif", "img/rse/");


		$this->addLargeButton('submit', 'Uložiť', "$('#edit_form').submit();");
		$this->addLargeButton("return", "Späť", "location.href='mod.rs.files.php'");
		//Content::$Mainbox.=\UI::ButtonsBar($Buttons);


	}

	function actionSave(ArrayHash $data){

		$id = $data->id;
		/** @var FileUpload $file */
		$file = $data->userfile;

		if($file->isOk()){
			$dstName = date( 'Y-m-d-His-', time() ) . $file->getSanitizedName(); //$newFileName;
			$content = ( file_get_contents( $file->getTemporaryFile() ) );
			$rx = $this->db->query( "select max(poradie)+1 as poradie from cms_file_data" )->fetch();

			$add = [
				"container" => $id,
				"bdata"     => $content,
				"poradie"   => $rx["poradie"],
				"filename"  => $dstName,
				"nazov"     => $data->filename,
			];

			$query = "INSERT INTO cms_file_data ";

			$this->db->query( $query, $add );

			unlink( $file->getTemporaryFile() );
			$this->flashMessage("Súbor bol nahratý");
		}

		$this->redirect("mod.rs.files.php?action=edit&id=".$id);

	}


	function actionDelete(int $id)
	{
		$this->db->query("DELETE FROM cms_html WHERE id = ?", $id);///->getQueryString();
		$this->redirect("mod.rs.content.php");
	}

	function actionDeleteFile(int $id, int $container)
	{
		$this->db->query("DELETE FROM cms_file_data WHERE id = ?", $id);
		$this->flashMessage("Súbor bol vymazaný", \Flash::TYPE_DANGER);
		$this->redirect("mod.rs.files.php?action=edit&id=" . $container);
	}
}