<?php
namespace Commerce\Manager\Controller;

use Auth;
use Cache;
use CmsMenu;
use Commerce\ControllerFactory;
use Content;
use Nette\Application\ForbiddenRequestException;
use Nette\Utils\ArrayHash;
use Page;
use PageData;

class RsPagesController extends ManagerController {
	public function startup(): void
	{
		parent::startup();
		if(! Auth::ACL('business_view')){
			throw new ForbiddenRequestException();
		}
	}

	function actionDefault(){

		$this->setTitle("Zoznam stránok", "img_pagemap.gif");

		$this->template->systemPages = Page::fetch(["system"=>1,"mobile"=>"0"],"main DESC,template ASC, title ASC");
		$this->template->otherPages = Page::fetch(["system"=>0,"mobile"=>"0"]," template DESC, title ASC");

		$this->addLargeButton("add","Pridať stránku", "location.href='mod.rs.pages.php?action=add'");
	}

	function actionAdd(){
		$this->setTitle("Nová stránka","img_pagemap.gif");
		$p = new Page();
		$p->title = "Nová stránka";
		$p->robots = "noindex,follow";
		$this->template->p = $p;
		$this->template->templates = [];
		foreach (\Template::fetch([], "id ASC") as $template){
			$this->template->templates[$template->id] = $template->nazov;
		}

		$this->addLargeButton('submit', 'Uložiť', "$('#edit_form').submit();");
		$this->addLargeButton("return","Späť","location.href='mod.rs.pages.php'");
	}

	function actionEdit(int $id){
		$p = new Page($id);

		$this->setTitle("Stránka - ".$p->title,"img_pagemap.gif");

		$this->addLargeButton('submit', 'Uložiť', "$('#edit_form').submit();");
		$this->addLargeButton("return","Späť","location.href='mod.rs.pages.php'");



		$boxes = $boxesidsToName=  [];

		foreach($p->template()->variables() as $var){
			$boxes[$var->params["id"]] = [];
		}

		foreach($p->template()->boxes() as $B){
			if(isset($B["boxid"])){
				$boxesidsToName[$B["boxid"]] = $B["info"];
			}
		}
		$d = PageData::fetch(["page"=>$p->id],"poradie ASC");
		foreach($d as $data){
			$boxes[$data->box][] = $data;
		}
		$this->template->p = $p;
		$this->template->OBJ = $this->context->getByType(ControllerFactory::class);
		$this->template->boxes = $boxes;
		$this->template->boxesidsToName = $boxesidsToName;

		$this->template->systemPages = Page::fetch(["system"=>1,"mobile"=>"0"],"main DESC,template ASC, title ASC");
		$this->template->otherPages = Page::fetch(["system"=>0,"mobile"=>"0"]," template DESC, title ASC");

	}

	function actionSave(ArrayHash $X){
		$p = new Page($X["id"]);
		//dump($X,$p);
		$p->title = $X["title"];
		$p->desc  = $X["desc"];
		$p->url  = $X["url"];
		$p->robots  = $X["robots"];
		$p->microdata  = $X["microdata"];
		$p->system = $X["system"] ?? 0;
		if(!isset($X["id"])){
			$p->template = $X["template"];
		}
		//$p->main = 1;
		$p->mobile = 0;
		//$p->title = $X["title"];
		$p->save();
		$this->flashMessage("Stránka bola uložená.");
		$this->redirect("mod.rs.pages.php?action=edit&id=".$p->id);


	}

	function actionDelete(int $id){
		$p = new Page($id);
		$p->delete();
		$this->flashMessage("Stránka bola vymazaná.", \Flash::TYPE_DANGER);
		$this->redirect("mod.rs.pages.php");
	}

	public function actionSaveObject(ArrayHash $data){

		if(empty($data->object)){
			$this->flashMessage("Vyberte aspon jeden objekt", \Flash::TYPE_DANGER);
			$this->redirect("mod.rs.pages.php?action=add_object&page_id=".$data->page_id."&box=".$data->box);
		}
		$q=$this->db->query("SELECT max(poradie) as newporadie FROM cms_pages_data WHERE page = ? and box = ?",$data["page_id"],$data["box"])->fetch();
		$newporadie = $q["newporadie"] + 1;

		foreach ($data->object as $object){
			$this->db->query("INSERT INTO cms_pages_data",["page"=>$data["page_id"],"box"=>$data["box"],"object"=>$object,"poradie"=>$newporadie,"show"=>"hide"]);
			$newporadie++;
		}

		Cache::flush("cms.templates.data");
		Cache::flush("cms.templates");
		Cache::flush("cms.pages.data");
		Cache::flush("cms.pages");

		$this->flashMessage("Objekt bol pridaný.");

		$this->redirect("mod.rs.pages.php?action=edit&id=".$data->page_id."#box_".$data["box"]);
	}

	function actionAddObject(int $page_id, string $box)
	{
		$p                   = new Page($page_id);
		$this->template->p   = $p;
		$this->template->box = $box;

		$this->setTitle("Stránka - " . $p->title . " - pridanie objektu do " . $box, "img_template");
		$this->addLargeButton('submit', 'Uložiť', "$('#edit_form').submit();");
		$this->addLargeButton("return", "Späť", "location.href='mod.rs.pages.php?action=edit&id={$p->id}'");
		$this->template->vals = [];
		foreach ($this->context->getByType(ControllerFactory::class) as $k => $v) {
			$this->template->vals[$k] = '<img src="img/rse/' . $v->objIcon . '"> ' . $v->objName;
		}

	}

}