<?php


namespace Commerce\Manager\Controller;

use Nette\Http\Url;
use Nette\Utils\Strings;


class DefaultController extends ManagerController {
	public function startup(): void
	{
		parent::startup();

		if(($m = $this->httpRequest->getQuery('m')) !== null) {
			\AJAX::call($m);
		}

		if($this->user->isLoggedIn()){
			if(\Auth::$mainPage == Null){
				$this->redirect("mod.objednavky.php?reset=1&search=1&autorefresh=1");
			} else {
				$this->redirect(\Auth::$mainPage);
			}
		}
	}

	public function actionDefault() {

	}
}