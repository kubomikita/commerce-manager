<?php

namespace Commerce\Manager\Controller;

use Auth;
use Content;
use Nette\Application\ForbiddenRequestException;
use Nette\Http\FileUpload;
use Nette\Utils\ArrayHash;
use PartnerTag;
use TovarVyrobca;

class PartneriTagyController extends ManagerController
{

	public function startup(): void
	{
		parent::startup();
		if(! Auth::ACL('clients_admin')){
			throw new ForbiddenRequestException();
		}
	}

	public function actionDefault(){
		$this->setTitle("Vlastnosti partnerov (štítky)", "tag");
		$this->addLeftButton('add','Pridať vlastnosť', "location.href='mod.partneri.tagy.php?action=edit&id=0';");
		$this->template->list = PartnerTag::fetch();
	}


	function actionEdit(int $id){

		$PT=new PartnerTag($id);
		if(!is_array($PT->metadata["vyrobca"])) {
			$PT->metadata["vyrobca"] = array();
		}

		$this->template->PT = $PT;
		$this->template->vyrobcovia = TovarVyrobca::fetch();
		$this->template->id = $id;

		$this->setTitle("Vlastnost - ".($id?LangStr('ÚPRAVA'):LangStr('NOVÁ')), "tag");
		$this->addLeftButton("submit","Zapísať zmeny","$('#mainform').submit();");
		if($PT->id !== null){
			$this->addLeftButtonConfirm("delete","Zmazať","location.href='mod.partneri.tagy.php?action=delete&id=".$PT->id."';");
		}
		$this->addLeftButton("return","Návrat","location.href='mod.partneri.tagy.php';");

	}

	function actionSave(ArrayHash $X){
		if($X['id']){ $PT=new PartnerTag($X['id']); } else { $PT=new PartnerTag(); };
		$PT->skratka=$X['skratka'];
		$PT->nazov=$X['nazov'];
		$PT->popis=$X['popis'];
		$PT->metadata = $X["metadata"];

		$PT->save();
		$this->flashMessage("Vlastnosť bola uložená.");
		$this->redirect("mod.partneri.tagy.php");
	}

	function actionDelete(int $id){
		if(!Auth::ACL('clients_admin')){ return false; };
		if($id > 0){
			$PT=new PartnerTag($id);
			$PT->delete();
		}
		$this->flashMessage("Vlastnosť bola vymazaná.", \Flash::TYPE_DANGER);
		$this->redirect("mod.partneri.tagy.php");
	}

}