<?php

namespace Commerce\Manager\Controller;

use Commerce\Manager\Manager;
use Content;
use JetBrains\PhpStorm\NoReturn;
use Kubomikita\Commerce\ComponentModel;
use Kubomikita\Commerce\ConfiguratorInterface;
use Kubomikita\Commerce\UserService;
use Kubomikita\Flash;
use Latte\Engine;
use Nette\Application\Routers\Route;
use Nette\Database\Connection;
use Nette\Database\Explorer;
use Nette\DI\Container;
use Nette\Http\IRequest;
use Nette\Http\IResponse;
use Nette\Security\User;
use Nette\Utils\Strings;
use ArrayAccess;

abstract class ManagerController extends ComponentModel {
	protected ConfiguratorInterface $context;
	protected \stdClass $template;
	protected ?User $user;
	protected ?UserService $userService;
	protected ?string $action; // = "default";
	protected Connection $db;
	protected array $ui = [];


	public function __construct(
		protected Container $container,
		protected IRequest $httpRequest,
		protected IResponse $httpResponse,
		protected Engine $latte,
		protected Explorer $explorer,
	)
	{
		$this->context = $this->container->getByType(ConfiguratorInterface::class);
		$this->db = $this->explorer->getConnection();
		$this->template = new \stdClass();

	}

	public function startup() : void
	{
		/*Flash::options([
			"types" => [
				"success" => ["image" => "img/16/bool_1.png", "style" => 'font-weight:bold'],
				"danger" => ["image" => "img/16/alert.png", "style" => 'font-weight:bold'],
				"info" => ["image" => "img/16/info.png", "style" => 'font-weight:bold'],
				"warning" => ["image" => "img/16/warning.png", "style" => 'font-weight:bold'],
			]
		]);*/
		//dumpe($this, $this->container->getByType(Manager::class)->isRedirectWww());
		if($this->context->isProduction() && $this->container->getByType(Manager::class)->isRedirectWww()){
			$host = $this->httpRequest->getUrl()->getHost();
			if(!Strings::startsWith($host, 'www.')){
				$this->redirect($this->httpRequest->getUrl()->withHost('www.'.$host));
			}
		}
	}

	protected function beforeRender()
	{
		$this->template->manager = $this->container->getByType(Manager::class);
		$this->template->basePath = $this->httpRequest->getUrl()->getBasePath();
		$this->template->presenter = $this->template->controller = $this;
		$this->template->user = $this->user;
		$this->template->parameters = $this->container->parameters;
		$this->template->menu = $this->createMenu();
		$this->template->logoutUrl = $this->httpRequest->getUrl()->withQuery(['logout' => 1]);

		foreach ($this->ui as $key => $value){
			$this->template->ui[$key] = implode(" ", $value);
		}

	}

	public function sendResponse() : void {
		if(($template = $this->formatTemplateFile()) !== null){
			$this->beforeRender();
			$this->latte->render($template, $this->getTemplateParameters());
		}

	}

	#[NoReturn]
	public function redirect(string $url, int $code = IResponse::S302_FOUND) :void
	{
		if($url === "this"){

			$url = $this->httpRequest->getUrl();
			//dumpe($url);
		}

		if($this->httpRequest->isAjax()){
			echo '<script>window.location.href=\''.$url.'\'</script>';
			exit;
		}


		$this->httpResponse->redirect($url, $code);
		$this->terminate();
	}

	#[NoReturn]
	public function terminate() : void
	{
		exit;
	}

	protected function createMenu() : \Menu {
		$Menu = new \Menu();
		include/*_once*/ APP_DIR.'manager/menu.php';
		//bdump($Menu);
		return $Menu;
	}

	protected function getTemplateParameters() : array
	{
		$parameters = [];
		$parameters['adminLayout'] = __DIR__.'/templates/@layout.latte';
		foreach ($this->template as $key => $value){
			$parameters[$key] = $value;
		}
		return $parameters;
	}

	public function formatTemplateFile(?string $view = null) : ?string
	{
		//dumpe(Route::path2action($this->action));
		$reflection = new \ReflectionClass($this);
		if (!str_starts_with($reflection->getFileName(), __DIR__)) {
			$pathinfo = pathinfo($reflection->getFileName());
			$dir = $pathinfo['dirname'].'/templates/'.str_replace("Controller", "", $reflection->getShortName())."/".Route::path2action(($view ?? $this->action)).".latte";

			if (file_exists($dir)) {
				return $dir;
			}
		}

		return __DIR__."/templates/".str_replace("Controller", "", $reflection->getShortName())."/".Route::path2action(($view ?? $this->action)).".latte";
	}

	public function setUser(?User $user): static
	{
		$this->user = $user;
		return $this;
	}

	public function setUserService(?UserService $userService): static
	{
		$this->userService = $userService;
		return $this;
	}

	public function setAction(?string $action): static
	{
		$this->action = $action;
		return $this;
	}

	/** Admin template UI */

	protected function setTitle(string $title, ?string $icon = null, ?string $iconPath = 'img/32/') : void {

		$ico = $icon === null ? null : $iconPath.$icon;
		if($ico !== null && !file_exists($iconPath.$icon)){
			$ico .= '.png';
		}
		
		Content::$titleIcon = $ico;
		Content::$titleText = __($title);
	}
	protected function flashMessage(string $message, string $type = Flash::TYPE_SUCCESS) : void
	{
		Flash::add($type, $message);
	}
	protected function addLeftButton(string $icon, string $text, string $href) : void
	{
		Content::$Leftbox.= Content::leftButton('img/32/'.$icon.'.png',__($text), $href); //
	}
	protected function addLeftButtonConfirm(string $icon, string $text, string $href) : void
	{
		Content::$Leftbox.= Content::leftButtonConfirm('img/32/'.$icon.'.png',__($text), $href); //
	}

	protected function addLargeButton(string $icon, string $text, string $href): void
	{
		$this->ui["ButtonsBar"][] = \UI::LargeButton('img/32/' . $icon . '.png', __($text), $href);
	}

	public function addTab(string $id, string $name, bool $isDynamic= false, array $params = []) :void
	{
		Content::addTab($id, __($name));
		$this->ui["tabs"]["params"][$id] = $params;
		//if($isDynamic) {
			$this->ui["tabs"]["dynamic"][$id] = $isDynamic;
		//}
	}

	public function tabsControl() : array
	{
		$rendered = [];
		foreach (Content::$tabsIndex as $tab){

			$id            = $tab["id"];
			$params        = $this->ui["tabs"]["params"][$id] ?? [];
			$dynamic       = $this->ui["tabs"]["dynamic"][$id] ?? false;
			$array         = explode("_", $id);
			$dynamicPrefix = reset($array);

			if ($dynamic && Strings::contains($id, $dynamicPrefix."_")){
				$method = "action" . Route::path2presenter($dynamicPrefix);
				//dump($method);
				if(method_exists($this, $method)){
					$this->{$method}(end($array));
				}
				/*dump($params,$dynamic, $dynamicPrefix);
				dump($this);*/
				$rendered[$id] = $this->latte->renderToString($this->formatTemplateFile($dynamicPrefix),$this->getTemplateParameters() + $params);
			} elseif($id !== "default"){
				$rendered[$id] = $this->latte->renderToString($this->formatTemplateFile($id),$this->getTemplateParameters() + $params);
			}
		}
		return $rendered;
	}
}